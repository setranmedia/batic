<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('domain')->unique();
            $table->integer('user_id')->unsigned();
            $table->string('theme');
            $table->integer('per_page')->default(10);
            $table->string('site_name');
            $table->longText('categories')->nullable();
            $table->longText('pages')->nullable();
            $table->longText('header_script')->nullable();
            $table->longText('footer_script')->nullable();
            $table->integer('seo_home_id')->nullable();
            $table->integer('seo_category_id')->nullable();
            $table->integer('seo_keyword_id')->nullable();
            $table->integer('seo_search_id')->nullable();
            $table->integer('template_home_id')->nullable();
            $table->integer('template_category_id')->nullable();
            $table->integer('template_keyword_id')->nullable();
            $table->integer('template_search_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains');
    }
}
