<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = config('import.categories');
        if(is_array($categories)){
        	foreach($categories as $category){
        		\App\Category::create($category);
        	}
        }
    }
}
