<?php

use Illuminate\Database\Seeder;
use App\Domain;
use App\Seo;
use App\Template;

class DefaultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seos = config('batic.defaultSeo');
        foreach($seos as $seo){
            $seo['name'] = 'Default '.$seo['type'];
            Seo::create($seo);
        }
        $templates = config('batic.defaultTemplate');
        foreach($templates as $template){
            $template['name'] = 'Default '.$template['type'];
            Template::create($template);
        }
        $domain = config('batic.defaultDomain');
        $domain['domain'] = config('btracker.domain');
        Domain::create($domain);
    }
}
