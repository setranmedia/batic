<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/install','Installation\Controller@index');
Route::post('/get-token',['as' => 'installation.getToken','uses' => 'Installation\Controller@getToken']);
Route::post('/migrate-database',['as' => 'installation.migrateDatabase','uses' => 'Installation\Controller@migrateDatabase']);
