<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',['as' => 'home', 'uses' => 'PublicWeb\HomeController@render']);
Route::get('/redirect',['as' => 'redirect', 'uses' => 'PublicWeb\RedirectController@render']);
Route::get('/search',['as' => 'search', 'uses' => 'PublicWeb\SearchController@render']);
Route::get('/robots.txt',['as' => 'robots.txt', 'uses' => 'PublicWeb\Sitemap\RobotsController@render']);

$permalinks = config('batic.permalinks');
Route::get($permalinks['category'],['as' => 'category', 'uses' => 'PublicWeb\CategoryController@render']);
Route::get($permalinks['keyword'],['as' => 'keyword', 'uses' => 'PublicWeb\KeywordController@render']);
Route::get($permalinks['page'],['as' => 'page', 'uses' => 'PublicWeb\PageController@render']);

$sitemaps = config('batic.sitemaps');
Route::get('sitemap.xml',['as' => 'sitemap.index', 'uses' => 'PublicWeb\Sitemap\IndexController@render']);
Route::get('sitemap-'.$sitemaps['main_sitemap'].'.xml',['as' => 'sitemap.main', 'uses' => 'PublicWeb\Sitemap\MainController@render']);
Route::get('sitemap-'.$sitemaps['catalog_sitemap'].'-{page}.xml',['as' => 'sitemap.keyword', 'uses' => 'PublicWeb\Sitemap\KeywordController@render']);

Route::get('/ajax/checkAlive',['as' => 'public.ajax.get.checkAlive', 'uses' => 'PublicWeb\AjaxGetController@checkAlive']);
