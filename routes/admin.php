<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login',['as' => 'admin.login', 'uses' => 'Admin\LoginController@showLoginForm']);
Route::post('/login',['as' => 'admin.authenticate', 'uses' => 'Admin\LoginController@login']);
Route::post('/logout',['as' => 'admin.logout', 'uses' => 'Admin\LoginController@logout']);

Route::get('/',['as' => 'admin.dashboard', 'uses' => 'Admin\PanelController@dashboard']);
Route::get('/domains',['as' => 'admin.domains', 'uses' => 'Admin\PanelController@domains']);
Route::get('/categories',['as' => 'admin.categories', 'uses' => 'Admin\PanelController@categories']);
Route::get('/pages',['as' => 'admin.pages', 'uses' => 'Admin\PanelController@pages']);
Route::get('/seos',['as' => 'admin.seos', 'uses' => 'Admin\PanelController@seos']);
Route::get('/templates',['as' => 'admin.templates', 'uses' => 'Admin\PanelController@templates']);
Route::get('/keywords',['as' => 'admin.keywords', 'uses' => 'Admin\PanelController@keywords']);
Route::get('/queries',['as' => 'admin.queries', 'uses' => 'Admin\PanelController@queries']);
Route::get('/settings/profile',['as' => 'admin.settings.profile', 'uses' => 'Admin\PanelController@settingProfile']);
Route::get('/settings/permalinks',['as' => 'admin.settings.permalinks', 'uses' => 'Admin\PanelController@settingPermalinks']);
Route::get('/settings/sitemaps',['as' => 'admin.settings.sitemaps', 'uses' => 'Admin\PanelController@settingSitemaps']);


Route::post('/ajax/profile',['as' => 'admin.ajax.get.profile', 'uses' => 'Admin\Ajax\GetController@profile']);
Route::post('/ajax/config',['as' => 'admin.ajax.get.config', 'uses' => 'Admin\Ajax\GetController@config']);
Route::post('/ajax/categories',['as' => 'admin.ajax.get.categories', 'uses' => 'Admin\Ajax\GetController@categories']);
Route::post('/ajax/pages',['as' => 'admin.ajax.get.pages', 'uses' => 'Admin\Ajax\GetController@pages']);
Route::post('/ajax/seos',['as' => 'admin.ajax.get.seos', 'uses' => 'Admin\Ajax\GetController@seos']);
Route::post('/ajax/templates',['as' => 'admin.ajax.get.templates', 'uses' => 'Admin\Ajax\GetController@templates']);
Route::post('/ajax/themes',['as' => 'admin.ajax.get.themes', 'uses' => 'Admin\Ajax\GetController@themes']);
Route::post('/ajax/creatives',['as' => 'admin.ajax.get.creatives', 'uses' => 'Admin\Ajax\GetController@creatives']);

Route::post('/ajax/updateProfile',['as' => 'admin.ajax.post.updateProfile', 'uses' => 'Admin\Ajax\PostController@updateProfile']);
Route::post('/ajax/updatePassword',['as' => 'admin.ajax.post.updatePassword', 'uses' => 'Admin\Ajax\PostController@updatePassword']);
Route::post('/ajax/updateBatic',['as' => 'admin.ajax.post.updateBatic', 'uses' => 'Admin\Ajax\PostController@updateBatic']);
Route::post('/ajax/updateBatic',['as' => 'admin.ajax.post.updateBatic', 'uses' => 'Admin\Ajax\PostController@updateBatic']);

Route::post('/ajax/createDomain',['as' => 'admin.ajax.post.createDomain', 'uses' => 'Admin\Ajax\PostController@createDomain']);
Route::post('/ajax/updateDomain',['as' => 'admin.ajax.post.updateDomain', 'uses' => 'Admin\Ajax\PostController@updateDomain']);
Route::post('/ajax/deleteDomain',['as' => 'admin.ajax.post.deleteDomain', 'uses' => 'Admin\Ajax\PostController@deleteDomain']);
Route::post('/ajax/flushDomain',['as' => 'admin.ajax.post.flushDomain', 'uses' => 'Admin\Ajax\PostController@flushDomain']);

Route::post('/ajax/createCategory',['as' => 'admin.ajax.post.createCategory', 'uses' => 'Admin\Ajax\PostController@createCategory']);
Route::post('/ajax/updateCategory',['as' => 'admin.ajax.post.updateCategory', 'uses' => 'Admin\Ajax\PostController@updateCategory']);
Route::post('/ajax/deleteCategory',['as' => 'admin.ajax.post.deleteCategory', 'uses' => 'Admin\Ajax\PostController@deleteCategory']);

Route::post('/ajax/createPage',['as' => 'admin.ajax.post.createPage', 'uses' => 'Admin\Ajax\PostController@createPage']);
Route::post('/ajax/updatePage',['as' => 'admin.ajax.post.updatePage', 'uses' => 'Admin\Ajax\PostController@updatePage']);
Route::post('/ajax/deletePage',['as' => 'admin.ajax.post.deletePage', 'uses' => 'Admin\Ajax\PostController@deletePage']);

Route::post('/ajax/createSeo',['as' => 'admin.ajax.post.createSeo', 'uses' => 'Admin\Ajax\PostController@createSeo']);
Route::post('/ajax/updateSeo',['as' => 'admin.ajax.post.updateSeo', 'uses' => 'Admin\Ajax\PostController@updateSeo']);
Route::post('/ajax/deleteSeo',['as' => 'admin.ajax.post.deleteSeo', 'uses' => 'Admin\Ajax\PostController@deleteSeo']);

Route::post('/ajax/createTemplate',['as' => 'admin.ajax.post.createTemplate', 'uses' => 'Admin\Ajax\PostController@createTemplate']);
Route::post('/ajax/updateTemplate',['as' => 'admin.ajax.post.updateTemplate', 'uses' => 'Admin\Ajax\PostController@updateTemplate']);
Route::post('/ajax/deleteTemplate',['as' => 'admin.ajax.post.deleteTemplate', 'uses' => 'Admin\Ajax\PostController@deleteTemplate']);

Route::post('/ajax/createQueue',['as' => 'admin.ajax.post.createQueue', 'uses' => 'Admin\Ajax\PostController@createQueue']);
Route::post('/ajax/deleteQueue',['as' => 'admin.ajax.post.deleteQueue', 'uses' => 'Admin\Ajax\PostController@deleteQueue']);
Route::post('/ajax/deleteKeyword',['as' => 'admin.ajax.post.deleteKeyword', 'uses' => 'Admin\Ajax\PostController@deleteKeyword']);

Route::post('/ajax/grabProductsCategory',['as' => 'admin.ajax.post.grabProductsCategory', 'uses' => 'Admin\Ajax\PostController@grabProductsCategory']);


Route::post('/ajax/datatableDomains',['as' => 'admin.ajax.datatable.domains', 'uses' => 'Admin\Ajax\DataTableController@domains']);
Route::post('/ajax/datatableCategories',['as' => 'admin.ajax.datatable.categories', 'uses' => 'Admin\Ajax\DataTableController@categories']);
Route::post('/ajax/datatablePages',['as' => 'admin.ajax.datatable.pages', 'uses' => 'Admin\Ajax\DataTableController@pages']);
Route::post('/ajax/datatableSeos',['as' => 'admin.ajax.datatable.seos', 'uses' => 'Admin\Ajax\DataTableController@seos']);
Route::post('/ajax/datatableTemplates',['as' => 'admin.ajax.datatable.templates', 'uses' => 'Admin\Ajax\DataTableController@templates']);
Route::post('/ajax/datatableKeywords',['as' => 'admin.ajax.datatable.keywords', 'uses' => 'Admin\Ajax\DataTableController@keywords']);
Route::post('/ajax/datatableQueries',['as' => 'admin.ajax.datatable.queries', 'uses' => 'Admin\Ajax\DataTableController@queries']);
Route::post('/ajax/datatableQueues',['as' => 'admin.ajax.datatable.queues', 'uses' => 'Admin\Ajax\DataTableController@queues']);
