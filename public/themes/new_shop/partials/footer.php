<div class="footer-w3l">
	<div class="container">
		<div class="footer-grids">
			<div class="col-md-3 footer-grid">
				<h4>Information</h4>
				<ul>
					<li><a href="<?php echo route('home'); ?>">Home</a></li>
					<?php
						$pages = config('pages');
						if($pages){
							foreach ($pages as $page) {
								echo '<li><a href="'.$page->url.'">'.$page->name.'</a></li>';
							}
						}
					?>
				</ul>
				<div class="social-icon">
					<a href="#"><i class="icon"></i></a>
					<a href="#"><i class="icon1"></i></a>
					<a href="#"><i class="icon2"></i></a>
					<a href="#"><i class="icon3"></i></a>
				</div>
			</div>
			<div class="col-md-9 footer-grid">
				<h4>Most Searches</h4>
				<div class="most-searches" style="color:#eee;">
				<?php
					$keywords = random_keywords(20);
					if($keywords){
						$newKeywords = [];
						foreach ($keywords as $keyword) {
							$newKeywords[] = '<a href="'.$keyword->url.'">'.$keyword->query.'</a>';
						}
						echo implode(', ', $newKeywords);
					}
				?>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<div class="copy-section">
	<div class="container">
		<div class="copy-left">
			<p>&copy; <?php echo date('Y'); ?> <?php echo config('site.site_name'); ?>. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
		</div>
		<div class="copy-right"><img src="<?php echo Theme::asset()->url('images/card.png');?>" alt=""/></div>
		<div class="clearfix"></div>
	</div>
</div>
<?php echo config('site.footer_script'); ?>
<script type="text/javascript">
	jQuery(".redirect").click(redirectOffer);
</script>