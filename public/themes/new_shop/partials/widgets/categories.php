<?php $categories = config('categories'); ?>
<div class="categories">
	<h3>Categories</h3>
	<ul class="tree-list-pad">
		<?php if($categories): ?>
			<?php if(is_layout(['home','keyword','search'])): ?>
				<?php foreach ($categories as $category): ?>
					<li><a href="<?php echo route('category',[$category->slug]); ?>"><?php echo $category->name; ?></a></li>
				<?php endforeach;?>
			<?php elseif(is_layout('category')): ?>
				<?php
					$cat =  config('category');
					$childs = $cat->childs;
				?>
				<?php foreach ($categories as $category): ?>
					<?php if(($category->id == $cat->id || $category->id == $cat->parent_id)): ?>
						<?php foreach ($category->childs as $child): ?>
							<li><a href="<?php echo route('category',[$child->slug]); ?>"><?php echo $child->name; ?></a></li>
						<?php endforeach;?>
					<?php elseif(!$childs): ?>
						<li><a href="<?php echo route('category',[$category->slug]); ?>"><?php echo $category->name; ?></a></li>
					<?php endif; ?>
				<?php endforeach;?>
			<?php endif; ?>
		<?php endif; ?>
	</ul>
</div>