<head>
    <meta charset="utf-8">
    <?php echo SEO::generate(); ?>
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Theme::asset()->url('css/bootstrap.css');?>">
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Theme::asset()->url('css/style.css');?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="<?php echo Theme::asset()->url('img/favicon.ico');?>" rel="icon" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href='//fonts.googleapis.com/css?family=Cagliostro' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,600,400italic,300italic,300' rel='stylesheet' type='text/css'>

    <script src="<?php echo Theme::asset()->url('js/jquery.min.js');?>"></script>
    <script src="<?php echo Theme::asset()->url('js/main.js');?>"></script>
    <script src="<?php echo Theme::asset()->url('js/bootstrap-3.1.1.min.js');?>"></script>
    <?php echo config('site.header_script'); ?>
</head>