<div class="header">
	<div class="heder-bottom">
		<div class="container">
			<div class="logo-nav">
				<div class="logo-nav-left">
					<h2><a href="<?php echo route('home'); ?>"><?php echo config('site.site_name'); ?></a></h2>
				</div>
				<div class="logo-nav-right">
					<ul class="cd-header-buttons">
						<li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
					</ul>
					<div id="cd-search" class="cd-search">
						<form action="<?php echo route('search'); ?>" method="get">
							<input name="q" type="search" placeholder="Search...">
						</form>
					</div>	
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>