<?php if(is_array($products)): ?>
<?php echo Theme::partial('ads.before-product'); ?>
<?php foreach ($products as $product): ?>
<div class="product-tab1" style="margin-bottom: 20px;border: solid;padding: 5px;">
	<div class="col-md-4 product-tab1-grid">
		<div class="grid-arr">
			<div class="grid-arrival">
				<figure>
					<a href="#" class="redirect new-grid" data-url="<?php echo base64_encode($product['url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>">
						<div class="grid-img">
							<img  src="<?php echo str_replace('http://','https://',$product['image']); ?>_200x200.jpg" class="img-responsive" alt="<?php echo $product['title']; ?>">
						</div>
					</a>
				</figure>
			</div>
		</div>
		<div class="women" style="margin-top: 10px;">
			<p class="text-center">
				<a class="redirect" href="#" title="<?php echo $product['suppliers']['name']; ?>" data-url="<?php echo base64_encode($product['suppliers']['url']); ?>" data-title="<?php echo base64_encode($product['suppliers']['name']); ?>"><em class="item_price" style="font-size: x-small;"><?php echo $product['suppliers']['name']; ?></em></a>
			</p>
			<p class="text-center">

			</p>
		</div>
	</div>
	<div class="col-md-8 product-tab1-grid1 simpleCart_shelfItem">
		<div class="women">
			<h3><a><?php echo $product['title']; ?></a></h3>
			<em class="item_price"><?php echo $product['price']['currency_text'].$product['price']['low']; ?> - <?php echo $product['price']['high']; ?>/<?php echo $product['price']['piece']; ?></em>
			<em class="item_price" style="font-size: x-small;">(Min. Order: <?php echo $product['min_order']?>)</em>
			<table class="table table-striped" style="margin-top: 10px;">
				<?php if(isset($product['details']) && is_array($product['details'])): ?>
					<?php foreach ($product['details'] as $detail): ?>
						<tr style="font-size: small;">
							<td style="vertical-align:middle;padding: 5px!important;"><strong><?php echo $detail['name']; ?></strong></td>
							<td style="vertical-align:middle;padding: 5px!important;">:</td>
							<td style="vertical-align:middle;padding: 5px!important;"><?php echo $detail['value']; ?></td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</table>
			<div class="text-center">
				<button class="redirect btn btn-danger my-cart-b" data-url="<?php echo base64_encode($product['contact_url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send Inquiry about this product">Send Inquiry</button>
				<button class="redirect btn btn-danger my-cart-b" data-url="<?php echo base64_encode($product['suppliers']['contact_url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send message to <?php echo ($product['suppliers']['name']); ?>">Contact Supplier</button>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php endforeach; ?>
<?php echo Theme::partial('ads.after-product'); ?>
<?php endif; ?>
