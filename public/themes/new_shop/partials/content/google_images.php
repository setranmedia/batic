<?php if(is_array($google_images)): foreach ($google_images as $product): ?>
<div class="product-tab1" style="margin-bottom: 20px;border: solid;padding: 5px;">
	<div class="col-md-3 product-tab1-grid">
		<div class="grid-arr">
			<div class="grid-arrival">
				<figure>
					<a href="#" class="redirect new-grid" data-url="<?php echo base64_encode($product['url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>">
						<div class="grid-img">
							<img  src="<?php echo $product['image']; ?>_200x200.jpg" class="img-responsive" alt="<?php echo $product['title']; ?>">
						</div>
					</a>
				</figure>
			</div>
		</div>
	</div>
	<div class="col-md-9 product-tab1-grid1 simpleCart_shelfItem">
		<div class="women">
			<h3><a><?php echo $product['title']; ?></a></h3>
			<div class="text-center">
				<button class="redirect btn btn-danger my-cart-b" data-url="<?php echo base64_encode($product['url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send Inquiry about this product">Send Inquiry</button>
				<button class="redirect btn btn-danger my-cart-b" data-url="<?php echo base64_encode($product['url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send details">See Details</button>
			</div>		
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php endforeach; endif; ?>