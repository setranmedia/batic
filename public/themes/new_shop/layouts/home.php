<!DOCTYPE html>
<html>
	<?php echo Theme::partial('head'); ?>
	<body>
		<?php echo Theme::partial('header'); ?>
		<div class="banner1"></div>
		<div class="content">
			<div class="products-agileinfo">
				<div class="container">
					<?php echo config('description'); ?>
					<div class="product-agileinfo-grids w3l">
						<div class="col-md-9 product-agileinfon-grid1">
							<?php echo Theme::partial('content.products',['products' => config('products')]); ?>
						</div>
						<div class="col-md-3 product-agileinfo-grid">
							<?php echo Theme::partial('sidebar'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php echo Theme::partial('footer'); ?>
	</body>
</html>