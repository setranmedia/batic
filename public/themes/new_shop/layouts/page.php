<!DOCTYPE html>
<html>
	<?php echo Theme::partial('head'); ?>
	<body>
		<?php echo Theme::partial('header'); ?>
		<?php echo Theme::partial('breadcrumb'); ?>
		<div class="content">
			<div class="products-agileinfo">
				<div class="container">
					<h1 class="tittle"><?php echo config('page.name'); ?></h1>
						<div class="col-md-offset-1 col-md-10 product-agileinfon-grid1">
							<div style="border: 7px double #fbb005;padding: 10px;margin-top: 10px;margin-bottom: 20px;">
								<?php echo config('page.content'); ?>
							</div>
						</div>
				</div>
			</div>
		</div>
		
		<?php echo Theme::partial('footer'); ?>
	</body>
</html>