<div class="navbar-wrapper2 navbar-fixed-top">
	<div class="container">
		<div class="navbar mtnav">
			<div class="container offset-3">
				<div class="navbar-header">
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<h2><a href="<?php echo route('home'); ?>"><?php echo config('site.site_name'); ?></a></h2>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="<?php echo route('home'); ?>">Home</a></li>
						<?php
							$pages = config('pages');
							if($pages){
								foreach ($pages as $page) {
									echo '<li><a href="'.$page->url.'">'.$page->name.'</a></li>';
								}
							}
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

