<?php $categories = config('categories'); ?>
<div class="filters">
	<div class="hotelstab2">
		<form action="<?php echo route('search'); ?>" method="get">
			<input name="q" type="search" class="form-control" placeholder="Search and hit enter...">
		</form>
	</div>
	<div class="filtertip">
		<div class="padding20">
			<h3>Categories</h3>
		</div>
		<div class="tip-arrow" style="bottom: -9px;"></div>
	</div>
	<ul class="tree-list-pad">
		<?php if($categories): ?>
			<?php if(is_layout(['home','keyword','search'])): ?>
				<?php foreach ($categories as $category): ?>
					<li style="list-style: none;"><a href="<?php echo route('category',[$category->slug]); ?>"><?php echo $category->name; ?></a></li>
				<?php endforeach;?>
			<?php elseif(is_layout('category')): ?>
				<?php
					$cat =  config('category');
					$childs = $cat->childs;
				?>
				<?php foreach ($categories as $category): ?>
					<?php if(($category->id == $cat->id || $category->id == $cat->parent_id)): ?>
						<?php foreach ($category->childs as $child): ?>
							<li style="list-style: none;"><a href="<?php echo route('category',[$child->slug]); ?>"><?php echo $child->name; ?></a></li>
						<?php endforeach;?>
					<?php elseif(!$childs): ?>
						<li style="list-style: none;"><a href="<?php echo route('category',[$category->slug]); ?>"><?php echo $category->name; ?></a></li>
					<?php endif; ?>
				<?php endforeach;?>
			<?php endif; ?>
		<?php endif; ?>
	</ul>
</div>