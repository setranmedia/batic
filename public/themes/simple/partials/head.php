<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo SEO::generate(); ?>
    <!-- Bootstrap -->
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Theme::asset()->url('css/bootstrap.css');?>">
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Theme::asset()->url('css/custom.css');?>">
    <!-- Fonts -->  
    <link href="http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:700,400,300,300italic" rel="stylesheet" type="text/css">   
    <!-- Font-Awesome -->
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Theme::asset()->url('css/font-awesome.css');?>">
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="assets/css/font-awesome-ie7.css" media="screen" /><![endif]-->
    <!-- jQuery --> 
    <script src="<?php echo Theme::asset()->url('js/jquery.min.js');?>"></script>
    <?php echo config('site.header_script'); ?>
  </head>