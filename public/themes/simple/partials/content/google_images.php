<?php if(is_array($google_images)): foreach ($google_images as $product): ?>
<div class="offset-2">
	<hr class="featurette-divider3">
	<div class="col-md-4 offset-0">
		<div class="listitem2">
			<a href="#" class="redirect" data-title="<?php echo base64_encode($product['title']); ?>" data-gallery="multiimages" data-toggle="lightbox" data-url="<?php echo base64_encode($product['url']); ?>"><img src="<?php echo $product['image']; ?>_220x220.jpg" alt=""></a>
			<div class="liover" style="width: 10%; height: 10%; background-color: rgb(0, 153, 204); position: absolute; top: 105px; left: 135.5px; opacity: 0;"></div>
		</div>
	</div>
	<div class="col-md-8 offset-0">
		<div class="itemlabel3">
			<div class="labelright">
				<div style="text-align:right;">
				<button class="redirect bookbtn mt1" data-url="<?php echo base64_encode($product['contact_url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send Inquiry about this product">Send Inquiry</button><br>
				<button class="redirect bookbtn mt1" data-url="<?php echo base64_encode($product['url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="See Details <?php echo ($product['title']); ?>">See Details</button>	
				</div>
			</div>
			<div class="labelleft2">
				<b><?php echo $product['title']; ?></b><br><br>
			</div>
		</div>
	</div>
</div>
<?php endforeach; endif; ?>