<?php if(is_array($products)): ?>
<?php foreach ($products as $product): ?>
<div class="offset-2">
	<hr class="featurette-divider3">
	<div class="col-md-4 offset-0">
		<div class="listitem2">
			<a href="#" class="redirect" data-title="<?php echo base64_encode($product['title']); ?>" data-gallery="multiimages" data-toggle="lightbox" data-url="<?php echo base64_encode($product['url']); ?>"><img src="<?php echo str_replace('http://','https://',$product['image']); ?>_220x220.jpg" alt=""></a>
			<div class="liover" style="width: 10%; height: 10%; background-color: rgb(0, 153, 204); position: absolute; top: 105px; left: 135.5px; opacity: 0;"></div>
		</div>
	</div>
	<div class="col-md-8 offset-0">
		<div class="itemlabel3">
			<div class="labelright">
				<span class="green size18"><b><?php echo $product['price']['currency_text'].$product['price']['low']; ?> - <?php echo $product['price']['high']; ?></b></span><br>
				<span class="size11 grey"><?php echo $product['price']['piece']; ?></span><br>
				<span class="size11 grey">(Min. Order: <?php echo $product['min_order']?>)</span>
				<br><br>
				<div style="text-align:right;">
				<button class="redirect bookbtn mt1" data-url="<?php echo base64_encode($product['contact_url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send Inquiry about this product">Send Inquiry</button><br>
				<button class="redirect bookbtn mt1" data-url="<?php echo base64_encode($product['suppliers']['contact_url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send message to <?php echo ($product['suppliers']['name']); ?>">Contact Supplier</button>
				</div>
			</div>
			<div class="labelleft2">
				<b><?php echo $product['title']; ?></b><br><br>
				<div class="grey">
					<?php if(isset($product['details']) && is_array($product['details'])): ?>
						<?php foreach ($product['details'] as $detail): ?>
							<strong><?php echo $detail['name']; ?></strong> : <?php echo $detail['value']; ?><br>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<?php endforeach; ?>
<div style="padding-bottom: 5px;"></div>
<?php endif; ?>
