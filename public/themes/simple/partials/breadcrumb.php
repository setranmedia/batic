<?php if(!is_layout('home')): ?>
	<?php $crumbs = [['label' => 'Home','url' => route('home')]];?>
	<?php
		if(is_layout('category')){
			$category = config('category');
			if($category->parent){
				$crumbs[] = ['label' => $category->parent->name,'url' => $category->parent->url];
			}
			$crumbs[] = ['label' => $category->name,'url' => $category->url];
		}elseif(is_layout('keyword')){
			$keyword = config('keyword');
			$crumbs[] = ['label' => ucwords($keyword->query),'url' => $keyword->url];
		}elseif(is_layout('search')){
			$search = config('search');
			$crumbs[] = ['label' => 'Search results for: "'.$search->query.'"','url' => ''];
		}elseif(is_layout('page')){
			$page = config('page');
			$crumbs[] = ['label' => $page->name,'url' => $page->url];
		}
	?>
	<div class="container breadcrub">
	    <div>
			<div class="left">
				<ul class="bcrumbs">
					<?php 
						foreach($crumbs as $i => $crumb){
							if ($i != (count($crumbs) - 1))
								echo '<li><a href="'.$crumb['url'].'">'.$crumb["label"].'</a></li><li>/</li>';
							else
								echo '<li class="active">'.$crumb["label"].'</li>';
						}
					?>					
				</ul>				
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="brlines"></div>
	</div>
<?php endif; ?>