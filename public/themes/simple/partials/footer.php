<div class="footerbgblack">
	<div class="container">		
		<div class="col-md-3">
			<span class="ftitleblack">Verified By</span>
			<div class="scont">
				<a href="#" class="social1b"><img src="<?php echo Theme::asset()->url('images/card.png');?>" alt=""></a>
			</div>
			<br>
			<span class="grey2">&copy; <?php echo date('Y'); ?> <?php echo config('site.site_name'); ?>. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></span>
		</div>
		<!-- End of column 1-->
		<div class="col-md-9">
			<span class="ftitleblack">Most Searches</span>
			<br><br>
			<ul class="footerlistblack">
			<?php
				$keywords = random_keywords(20);
				if($keywords){
					$newKeywords = [];
					foreach ($keywords as $keyword) {
						$newKeywords[] = '<li><a href="'.$keyword->url.'">'.$keyword->query.'</a></li>';
					}
					echo implode(', ', $newKeywords);
				}
			?>
			</ul>
		</div>
		<!-- End of column 2-->	
	</div>	
</div>
<?php echo config('site.footer_script'); ?>
<script type="text/javascript">
	jQuery(".redirect").click(redirectOffer);
</script>


