<!DOCTYPE html>
<html>
	<?php echo Theme::partial('head'); ?>
	<body id="top" class="thebg">
		<?php echo Theme::partial('header'); ?>
		<div class="container breadcrub">
			<div class="clearfix"></div>
			<div class="brlines"></div>
		</div>
		<div class="container">
			<section style="text-align:center;">
			<div style="margin-bottom:10px;"><?php echo config('description'); ?></div>
			<?php echo Theme::partial('ads.before-product'); ?>
			</section>
			<div class="container pagecontainer offset-0" style="padding-bottom: 10px;">
				<div class="col-md-3 offset-0">
					<?php echo Theme::partial('sidebar'); ?>
				</div>
				<div class="rightcontent col-md-9 offset-0">
					<?php echo Theme::partial('content.products',['products' => config('products')]); ?>
				</div>
			</div>
			<div style="padding: 10px;"></div>
			<?php echo Theme::partial('ads.after-product'); ?>
		</div>
		<?php echo Theme::partial('footer'); ?>
	</body>
</html>