<!DOCTYPE html>
<html>
	<?php echo Theme::partial('head'); ?>
	<body id="top" class="thebg">
		<?php echo Theme::partial('header'); ?>
		<?php echo Theme::partial('breadcrumb'); ?>
		<div class="container">
			<section style="text-align:center;">
				<div style="margin-bottom:10px;"><?php echo config('description'); ?></div>
				<?php echo Theme::partial('ads.before-product'); ?>
			</section>
			<div class="container pagecontainer offset-0">
				<div class="col-md-3 offset-0">
					<?php echo Theme::partial('sidebar'); ?>
				</div>
				<div class="rightcontent col-md-9 offset-0">
					<?php $search = config('search');
						if($search->type == 'products')
							echo Theme::partial('content.products',['products' => $search->results]);
						elseif($search->type == 'google_images')
							echo Theme::partial('content.google_images',['google_images' => $search->results]);
					?>
				</div>
			</div>
			<div style="padding: 10px;"></div>
			<?php echo Theme::partial('ads.after-product'); ?>
		</div>
		<?php echo Theme::partial('footer'); ?>
	</body>
</html>