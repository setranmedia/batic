<!DOCTYPE html>
<html>
	<?php echo Theme::partial('head'); ?>
	<body>
		<?php echo Theme::partial('header'); ?>
		<main>
		<section class="container">
		<?php echo Theme::partial('breadcrumb'); ?>
			<section style="text-align:center;margin-bottom:10px">
				<h2 style="font-size:15px;"><?php echo config('description'); ?></h2>
			</section>
			<?php echo Theme::partial('sidebar'); ?>
			<div class="section-cont">
			<?php $search = config('search');
				if($search->type == 'products')
					echo Theme::partial('content.products',['products' => $search->results]);
				elseif($search->type == 'google_images')
					echo Theme::partial('content.google_images',['google_images' => $search->results]);
			?>
			</div>
		</section>
		<?php echo Theme::partial('footer'); ?>
		</main>
	</body>
</html>