<!DOCTYPE html>
<html>
	<?php echo Theme::partial('head'); ?>
	<body>
		<?php echo Theme::partial('header'); ?>
		<main>
		<section class="container">
			<section style="text-align:center;margin-bottom:10px">
				<h2 style="font-size:15px;"><?php echo config('description'); ?></h2>
			</section>
			<?php echo Theme::partial('sidebar'); ?>
			<div class="section-cont">
				<?php echo Theme::partial('content.products',['products' => config('products')]); ?>
			</div>
		</section>
		<?php echo Theme::partial('footer'); ?>
		</main>
	</body>
</html>