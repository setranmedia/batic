<div class="footer-wrap">
	<!-- End of column 1-->
	<div class="footer-top">
	</div>
	<div class="container f-menu-list">
		<div class="container">
			<div class="centerfooter">
			<b>Most Searches</b>
				<ul class="footerlistblack">
				<?php
					$keywords = random_keywords(20);
					if($keywords){
						$newKeywords = [];
						foreach ($keywords as $keyword) {
							$newKeywords[] = '<li style="display:inline"><a href="'.$keyword->url.'">'.$keyword->query.'</a></li>';
						}
						echo implode(' - ', $newKeywords);
					}
				?>
				</ul>
			</div>
			<center>
				<a href="#"><img src="<?php echo Theme::asset()->url('images/card.png');?>" alt=""></a>
			</center>
		</div>
	</div>
	<!-- End of column 2-->	
	<div class="footer-bottom">
		<div class="container">
			<span class="grey2">&copy; <?php echo date('Y'); ?> <?php echo config('site.site_name'); ?>. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></span>
		</div>
	</div>
</div>
<?php echo config('site.footer_script'); ?>
<script type="text/javascript">
	jQuery(".redirect").click(redirectOffer);
</script>


