<header class="header">
	<div class="header-middle">
		<div class="container header-middle-cont">
			<div class="toplogo">
				<div class="navbar-header">
					<h2><a href="<?php echo route('home'); ?>"><?php echo config('site.site_name'); ?></a></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<nav class="topmenu">
				<ul class="mainmenu sections-show">
					<li><a href="<?php echo route('home'); ?>">Home</a></li>
						<?php
						$pages = config('pages');
						if($pages){
							foreach ($pages as $page) {
								echo '<li><a href="'.$page->url.'">'.$page->name.'</a></li>';
							}
						}
						?>
				</ul>
				<div class="topsearch">
					<a id="topsearch-btn" class="topsearch-btn" href="#"><i class="fa fa-search"></i></a>
					<form class="topsearch-form" action="<?php echo route('search'); ?>" method="get">
						<input name="q" type="text" placeholder="Search products">
						<button type="submit"><i class="fa fa-search"></i></button>
					</form>
				</div>
			</nav>
		</div>
	</div>
</header>