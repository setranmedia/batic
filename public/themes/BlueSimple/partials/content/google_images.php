<?php if(is_array($google_images)): foreach ($google_images as $product): ?>
	<div class="prod-items section-items">
		<div class="prodlist-i">
			<a href="#" class="redirect prodlist-i-img" data-title="<?php echo base64_encode($product['title']); ?>" data-gallery="multiimages" data-toggle="lightbox" data-url="<?php echo base64_encode($product['url']); ?>"><img src="<?php echo $product['image']; ?>_220x220.jpg" alt=""></a>
			<div class="prodlist-i-cont">
				<b><?php echo $product['title']; ?></b><br><br>
			</div>
			<div class="prodlist-i-props-wrap">
				<ul class="prodlist-i-props">
				<button class="redirect prodlist-i-add" data-url="<?php echo base64_encode($product['contact_url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send Inquiry about this product">Send Inquiry</button><br>
				<button class="redirect prodlist-i-add" data-url="<?php echo base64_encode($product['url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="See Details <?php echo ($product['title']); ?>">See Details</button>
			</div>
		</div>
	</div>
<?php endforeach; endif; ?>