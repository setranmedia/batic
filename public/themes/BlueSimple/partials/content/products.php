<?php if(is_array($products)): ?>
<?php echo Theme::partial('ads.before-product'); ?>
<?php foreach ($products as $product): ?>
	<div class="prod-items section-items">
		<div class="prodlist-i">
			<a href="#" class="redirect prodlist-i-img" data-title="<?php echo base64_encode($product['title']); ?>" data-gallery="multiimages" data-toggle="lightbox" data-url="<?php echo base64_encode($product['url']); ?>"><img src="<?php echo str_replace('http://','https://',$product['image']); ?>_220x220.jpg" alt=""></a>
			<div class="prodlist-i-cont">
				<b><?php echo $product['title']; ?></b><br><br>
				<ul class="prodlist-i-props">
					<?php if(isset($product['details']) && is_array($product['details'])): ?>
						<?php foreach ($product['details'] as $detail): ?>
							<li><b><?php echo $detail['name']; ?></b> : <?php echo $detail['value']; ?></li>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div>
			<div class="prodlist-i-props-wrap">
				<ul class="prodlist-i-props">
				<li><b><?php echo $product['price']['currency_text'].$product['price']['low']; ?> - <?php echo $product['price']['high']; ?></b> /<?php echo $product['price']['piece']; ?></li>
				<li>(Min. Order: <?php echo $product['min_order']?>)</li>
				<button class="redirect prodlist-i-add" data-url="<?php echo base64_encode($product['contact_url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send Inquiry about this product">Send Inquiry</button><br>
				<button class="redirect prodlist-i-add" data-url="<?php echo base64_encode($product['suppliers']['contact_url']); ?>" data-title="<?php echo base64_encode($product['title']); ?>" title="Send message to <?php echo ($product['suppliers']['name']); ?>">Contact Supplier</button>
				</ul>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<?php echo Theme::partial('ads.after-product'); ?>
<?php endif; ?>
