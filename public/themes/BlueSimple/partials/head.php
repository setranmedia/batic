<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo SEO::generate(); ?>
    <meta content="follow, all" name="Slurp" />
    <meta content="follow, all" name="ia_archiver" />
    <meta content="follow, all" name="Baiduspider" />
    <meta content="follow, all" name="BecomeBot" />
    <meta content="follow, all" name="Bingbot" />
    <meta content="follow, all" name="btbot" />
    <meta content="follow, all" name="Dotbot" />
    <meta content="follow, all" name="Yeti" />
    <meta content="follow, all" name="Teoma" />
    <meta content="follow, all" name="Yandex" />
    <meta content="follow, all" name="FAST-WebCrawler" />
    <meta content="follow, all" name="FindLinks" />
    <meta content="follow, all" name="FyberSpider" />
    <meta content="follow, all" name="008" />
    <meta name="spiders" content="all" />
    <meta name="googlebot" content="All, index, follow" />
    <meta name="msnbot" content="All, index, follow" />
    <meta content="follow, all" name="robots" />
    <meta content="follow, all" name="seznambot" />
    <link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700ii%7CRoboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic" rel="stylesheet">
    <!-- Font-Awesome -->
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Theme::asset()->url('css/font-awesome.min.css');?>">
    <!-- Bootstrap -->
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Theme::asset()->url('css/bootstrap.min.css');?>">
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo Theme::asset()->url('css/style.css');?>">
    <!-- jQuery --> 
    <script src="<?php echo Theme::asset()->url('js/jquery.min.js');?>"></script>
    <script src="<?php echo Theme::asset()->url('js/main.js');?>"></script>
    <?php echo config('site.header_script'); ?>
  </head>