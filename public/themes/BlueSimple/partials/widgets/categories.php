<?php $categories = config('categories'); ?>
	<div class="section-sb">
		<div class="section-sb-current">
		<h3>Categories</h3>
		<ul class="section-sb-list" id="section-sb-list">
			<?php if($categories): ?>
				<?php if(is_layout(['home','keyword','search'])): ?>
					<?php foreach ($categories as $category): ?>
						<li class="categ-1"><a href="<?php echo route('category',[$category->slug]); ?>"><span class="categ-1-label"><?php echo $category->name; ?></span></a></li>
					<?php endforeach;?>
				<?php elseif(is_layout('category')): ?>
					<?php
						$cat =  config('category');
						$childs = $cat->childs;
					?>
					<?php foreach ($categories as $category): ?>
						<?php if(($category->id == $cat->id || $category->id == $cat->parent_id)): ?>
							<?php foreach ($category->childs as $child): ?>
								<li class="categ-1"><a href="<?php echo route('category',[$child->slug]); ?>"><span class="categ-1-label"><?php echo $child->name; ?></span></a></li>
							<?php endforeach;?>
						<?php elseif(!$childs): ?>
							<li class="categ-1"><a href="<?php echo route('category',[$category->slug]); ?>"><span class="categ-1-label"><?php echo $category->name; ?></span></a></li>
						<?php endif; ?>
					<?php endforeach;?>
				<?php endif; ?>
			<?php endif; ?>
		</ul>
		</div>
		<?php echo Theme::partial('ads.sidebar'); ?>
	</div>
