<!DOCTYPE html>
<html>
    @include('public.partials.head')
    <body class="hold-transition skin-blue fixed layout-top-nav">
        <div class="wrapper">
            @include('public.partials.header')
            <div class="content-wrapper">
                <div class="container">
                    <section class="content" id="main-content">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                <h1 class="text-center">{{config('redirect.title')}}</h1>
                                <div class="box box-info box-solid">
                                    <div class="box-body text-center">
                                      <div id="contact-url-loading">
                                        <h3>Please Wait...</h3>
                                        <p>Generating contact form</p>
                                        <img src="{{ url('assets/img/loading.gif') }}">
                                      </div>
                                      <div id="contact-url-ready" style="display:none;">
                                        <h3>Contact Form Generated</h3>
                                        <p>Clicking button below will redirect you to contact form</p>
                                        <a href="{!! config('redirect.url') !!}" class="btn btn-primary btn-lg">Continue <i class="fa fa-angle-double-right"></i></a>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            @include('public.partials.footer')
        </div>
    </body>
</html>
