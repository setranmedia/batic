@if(isset($google_images) && is_array($google_images))
	@foreach($google_images as $product)
		<div class="box box-info">
			<div class="box-body">
				<div class="row">
					<div class="col-md-3 text-center">
						<img src="{!! $product['image'] !!}" alt="{!! $product['title'] !!}" class="img-thumbnail">
					</div>
					<div class="col-md-9">
						<h2 class="box-title">{!! $product['title'] !!}</h2>
						<div class="text-center">			
							@if(isset($product['contact_url']))
								<a href="#" data-url="{!! base64_encode($product['contact_url']) !!}" data-title="{!! base64_encode($product['title']) !!}" title="Send Inquiry about this product" class="redirect btn btn-warning"><i class="fa fa-envelope"></i> Send Inquiry</a>
							@endif
							@if(isset($product['url']))
								<a data-url="{!! base64_encode($product['url']) !!}" data-title="{!! base64_encode($product['title']) !!}" title="See Details" class="redirect btn btn-primary"><i class="fa fa-eye"></i> See Details</a>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach
@endif