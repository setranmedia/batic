@if(isset($products) && $products)
	@foreach($products as $product)
		<div class="box box-info">
			<div class="box-header with-border">
				<h2 class="box-title">{!! $product['title'] !!}</h2>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-3 text-center">
						<img src="{!! $product['image'] !!}" alt="{!! $product['title'] !!}" class="img-thumbnail">
					</div>
					<div class="col-md-9">
						<h4>
							@if($product['price']['currency_text'])
								{!! $product['price']['currency_text'].$product['price']['low'] !!} - {!! $product['price']['high'] !!}/{!! $product['price']['piece'] !!}
							@endif
							<small>(Min. Order: {!! $product['min_order'] !!})</small>
						</h4>
						<table class="table table-striped" style="margin-top: 10px;">
						@if(isset($product['suppliers']['name']))
							<tr style="font-size: small;">
								<td style="vertical-align:middle;padding: 5px!important;"><strong>Supplier</strong></td>
								<td style="vertical-align:middle;padding: 5px!important;">:</td>
								<td style="vertical-align:middle;padding: 5px!important;"><a href="#" class="redirect" title="Visit suppliers site" data-url="{!! base64_encode($product['suppliers']['url']) !!}" data-title="{!! base64_encode($product['suppliers']['name']) !!}">{!! $product['suppliers']['name'] !!}</a></td>
							</tr>
						@endif
						@if(isset($product['details']) && is_array($product['details']))
							@foreach ($product['details'] as $detail)
								<tr style="font-size: small;">
									<td style="vertical-align:middle;padding: 5px!important;"><strong>{!! $detail['name'] !!}</strong></td>
									<td style="vertical-align:middle;padding: 5px!important;">:</td>
									<td style="vertical-align:middle;padding: 5px!important;">{!! $detail['value'] !!}</td>
								</tr>
							@endforeach
						@endif
						</table>
					</div>
				</div>
			</div>
			<div class="box-footer text-center">
				@if(isset($product['suppliers']['contact_url']))
					<a href="#" data-url="{!! base64_encode($product['suppliers']['contact_url']) !!}" data-title="{!! base64_encode($product['title']) !!}" title="Send message to {!! ($product['suppliers']['name']) !!}" class="redirect btn btn-info"><i class="fa fa-globe"></i> Contact Supplier</a>
				@endif
				
				@if(isset($product['contact_url']))
					<a href="#" data-url="{!! base64_encode($product['contact_url']) !!}" data-title="{!! base64_encode($product['title']) !!}" title="Send Inquiry about this product" class="redirect btn btn-warning"><i class="fa fa-envelope"></i> Send Inquiry</a>
				@endif
				@if(isset($product['url']))
					<a data-url="{!! base64_encode($product['url']) !!}" data-title="{!! base64_encode($product['title']) !!}" title="See Details" class="redirect btn btn-primary"><i class="fa fa-eye"></i> See Details</a>
				@endif
			</div>
		</div>
	@endforeach
@endif