@if($categories = config('categories'))
	@if(is_layout('category'))
		@if($cat =  config('category'))
			<div class="box box-solid hidden-sm hidden-xs">
				<div class="box-header with-border">
					<h2 class="box-title">{!! $cat->parent ? $cat->parent->name:$cat->name !!}</h2>
				</div>
				<div class="box-body no-padding" style="display: block;">
					<ul class="nav nav-pills nav-stacked">
						@foreach ($categories as $category)
							@if(($category->id == $cat->id || $category->id == $cat->parent_id))
								@foreach ($category->childs as $child)
									<li class="{!! $child->id == $cat->id ? 'active':'' !!}"><a href="{!! $child->url !!}">{!! $child->name !!}</a></li>
								@endforeach
							@elseif(!$childs = $cat->childs)
								<li><a href="{!! $category->url !!}"><{!! $category->name !!}</a></li>
							@endif
						@endforeach
					</ul>
				</div>
			</div>
		@endif
	@endif
	<div class="box box-solid hidden-sm hidden-xs">
		<div class="box-header with-border">
			<h2 class="box-title">Categories</h2>
		</div>
		<div class="box-body no-padding" style="display: block;">
			<ul class="nav nav-pills nav-stacked">
				@foreach($categories as $category)
					<li class="{!! isset($cat) && $category->id == $cat->id ? 'active':'' !!}"><a href="{!! $category->url !!}">{!! $category->name !!}</a></li>
				@endforeach
			</ul>
		</div>
	</div>
@endif