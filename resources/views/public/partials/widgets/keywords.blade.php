@if($keywords = random_keywords(20))
	<div class="box box-solid hidden-sm">
		<div class="box-header with-border">
			<h2 class="box-title">Most Searches</h2>
		</div>
		<div class="box-body no-padding" style="display: block;">
			<ul class="nav nav-pills nav-stacked">
				@foreach ($keywords as $keyword)
					<li><a href="{!! $keyword->url !!}">{!! $keyword->query !!}</a></li>
				@endforeach
			</ul>
		</div>
	</div>
@endif