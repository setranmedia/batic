<footer class="main-footer">
	<div class="container">
		<div class="pull-right hidden-xs">
			<strong>Copyright &copy; {!! date('Y') !!} <a href="{!! route('home') !!}">{!! config('site.domain') !!}</a>.</strong> All rights reserved.
		</div>
	</div>
</footer>
<script src="{{ url('assets/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ url('assets/dist/js/app.min.js') }}" type="text/javascript"></script>
{!! config('site.footer_script') !!}
<script type="text/javascript">
	@if(is_layout('redirect'))
		$(document).ready(function(){
			setTimeout(function(){
				$("#contact-url-loading").hide(1000);
				$("#contact-url-ready").show(1000);
			}, 5000);
		});
	@else
		jQuery(".redirect").click(redirectOffer);
	@endif
</script>
