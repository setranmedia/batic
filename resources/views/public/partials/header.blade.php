<header class="main-header">
	<nav class="navbar navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></button>
			</div>
			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="{!! route('home') !!}">Home</a></li>
					<li class="dropdown visible-xs">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							@foreach(config('categories') as $category)
								<li><a href="{!! $category->url !!}">{!! $category->name !!}</a></li>
							@endforeach
						</ul>
					</li>
				</ul>
			</div>
			<div class="navbar-custom-menu">
				<form class="navbar-form" role="search" method="get" style="padding: 0 15px;" action="{!! route('search') !!}">
					<div class="form-group">
						<input type="text" name="q" class="form-control" id="navbar-search-input" placeholder="Search">
					</div>
				</form>
			</div>
		</div>
	</nav>
</header>