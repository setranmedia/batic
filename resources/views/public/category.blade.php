<!DOCTYPE html>
<html>
    @include('public.partials.head')
    <body class="hold-transition skin-blue fixed layout-top-nav">
        <div class="wrapper">
            @include('public.partials.header')
            <div class="content-wrapper">
                <div class="container">
                    <section class="content" id="main-content">
                        <div class="row" style="padding: 25px 5px;">
                            <div class="col-md-12">
                                <?php echo config('description'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                @include('public.partials.content.products',['products' => config('products')])
                            </div>
                            <div class="col-md-3">
                                @include('public.partials.sidebar')
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            @include('public.partials.footer')
        </div>
    </body>
</html>