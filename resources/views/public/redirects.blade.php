<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	{!! SEO::generate() !!}
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/dist/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/dist/css/skins/_all-skins.min.css') }}">
	<meta http-equiv="refresh" content="5;URL='{!! config('redirect.url') !!}'" />
</head>
<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">
		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container">
					<div class="navbar-header"></div>
					<div class="navbar-custom-menu"><ul class="nav navbar-nav"></ul></div>
				</div>
			</nav>
		</header>
		<div class="content-wrapper">
			<div class="container">
				<section class="content">
					<div class="row">
						<div class="col-md-offset-3 col-md-6">
							<h1 class="text-center">{{config('redirect.title')}}</h1>
							<div class="box box-info box-solid">
								<div class="box-body text-center">
									<img src="{{ url('assets/img/loading.gif') }}">
								</div>
								<div class="box-footer text-center"><a href="{!! config('redirect.url') !!}">Click here</a>, If you are not redirected after 5 seconds.</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	<script src="{{ url('assets/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
	<script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ url('assets/dist/js/app.min.js') }}"></script>
</body>
</html>