<div class="box box-primary" v-if="database.status && btracker.status">
		<div class="box-header with-border">
			<h3 class="box-title">Complete</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<p>Selamat, BaTic berhasil terinstall. Tapi jangan lupa untuk merubah <span class="text-danger"><strong>RUN_INSTALLATION</strong></span> menjadi <span class="text-danger"><strong>false</strong></span> di file <span class="text-danger"><strong>.env</strong></span>. Setelah itu kamu bisa login ke dashboard admin dan merubah pengaturan lainnya menggunakan email dan password sevelumny</p>
					
					<div class="text-center">
						<a href="{!! url('admin') !!}" class="btn btn-success" target="_blank">Admin Dashboard</a>
					</div>
				</div>
			</div>
		</div>
	</div>