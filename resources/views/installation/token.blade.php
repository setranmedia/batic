<div class="box box-primary">
	<div class="box-header with-border"><h3 class="box-title">Token</h3></div>
	<div class="box-body">
		<div class="text-center">
			<a href="#" class="btn btn-success" data-toggle="modal" data-target="#getToken">Get Token</a>
		</div>
	</div>
	<div class="overlay" v-if="btracker.status">
		<i class="fa fa-check-square-o text-green"></i>
	</div>
</div>

<div id="getToken" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="post" class="form-horizontal form-bordered" v-on:submit="getToken">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" :disabled="btracker.process">&times;</button>
					<h4 class="modal-title">BTracker Tokenizer</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-md-4" for="btracker-email">BTracker.co Email</label>
						<div class="col-md-8">
							<input type="email" id="btracker-email" class="form-control" v-model="btracker.email" required="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4" for="btracker-password">BTracker.co Password</label>
						<div class="col-md-8">
							<input type="password" id="btracker-password" class="form-control" v-model="btracker.password" required="">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" :disabled="btracker.process">
						<i class="fa fa-refresh fa-spin" v-if="btracker.process"></i> Get Token
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" :disabled="btracker.process">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>