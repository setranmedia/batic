<div class="alert alert-danger">
	<h4><i class="icon fa fa-ban"></i> BaTic 2 System Requirements</h4>
	<p>BaTic 2 Not Supported by your system currently. Please Install or Upgrade some system requirements described below. After that, refresh this page and continue your installation steps.</p>
	<ul style="margin-top: 10px;">
		@foreach($requirements as $message)
			<li>{!! $message !!}</li>
		@endforeach
	</ul>
</div>