<div class="box box-primary" v-if="btracker.status">
	<div class="box-header with-border">
		<h3 class="box-title">Migrating Database</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<p>Pastikan detail Database yang anda isi di file <span class="text-danger"><strong>.env</strong></span>, sesuai yang tertera di bawah ini adalah benar. Dan pastikan pula bahwa database tersebut kosong alias baru, sebelum menjalankan proses migrasi database.</p>
				<table class="table table-striped">
					<tr>
						<td style="width: 40%;"><strong>Database Connection</strong></td>
						<td>@{{ database.connection }}</td>
					</tr>
					<tr v-if="database.connection=='mysql'">
						<td><strong>Database Host</strong></td>
						<td>@{{ database.host }}</td>
					</tr>
					<tr v-if="database.connection=='mysql'">
						<td><strong>Database Port</strong></td>
						<td>@{{ database.port }}</td>
					</tr>
					<tr v-if="database.connection=='mysql'">
						<td><strong>Database Name</strong></td>
						<td>@{{ database.name }}</td>
					</tr>
					<tr v-if="database.connection=='mysql'">
						<td><strong>Database Username</strong></td>
						<td>@{{ database.username }}</td>
					</tr>
					<tr v-if="database.connection=='mysql'">
						<td><strong>Database Password</strong></td>
						<td>@{{ database.password }}</td>
					</tr>
				</table>

				<div class="text-center">
					<a href="#" class="btn btn-success" data-toggle="modal" data-target="#migrateDatabase">Migrate Database</a>
				</div>
			</div>
		</div>
	</div>
	<div class="overlay" v-if="database.status">
		<i class="fa fa-check-square-o text-green"></i>
	</div>
</div>

<div id="migrateDatabase" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="post" class="form-horizontal form-bordered" v-on:submit="migrateDatabase">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" :disabled="migrating.process">&times;</button>
					<h4 class="modal-title">Database Migration</h4>
				</div>
				<div class="modal-body">
					<div class="@{{migrating.class}}" role="alert" v-if="migrating.submited">@{{migrating.message}}</div>
					<p>Dengan klik tombol <strong>Migrate</strong> maka akan menjalankan proses migrasi database, atau membuat table dan isian database yang diperlukan untuk menyimpan data BaTic.</p>
					<div class="form-group">
						<label class="control-label col-md-4" for="admin-name">Admin Name</label>
						<div class="col-md-8">
							<input type="text" id="admin-name" class="form-control" v-model="admin.name" required="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4" for="admin-email">Admin Email</label>
						<div class="col-md-8">
							<input type="email" id="admin-email" class="form-control" v-model="admin.email" required="">
						</div>
					</div>
					<div class="form-group @{{ admin.password.length < 6 ? 'has-error' : ''}}">
						<label class="control-label col-md-4" for="admin-password">Admin Password</label>
						<div class="col-md-8">
							<input type="password" id="admin-password" class="form-control" v-model="admin.password" required="">
							<span class="help-block" v-if="admin.password.length < 6">Minimal 6 karakter</span>
						</div>
					</div>
					<div class="form-group @{{ admin.password != admin.repassword ? 'has-error' : ''}}">
						<label class="control-label col-md-4" for="admin-repassword">Retype Password</label>
						<div class="col-md-8">
							<input type="password" id="admin-repassword" class="form-control" v-model="admin.repassword" required="">
							<span class="help-block" v-if="admin.password != admin.repassword">Password tidak sama</span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" :disabled="migrating.process || migrating.success">
						<i class="fa fa-refresh fa-spin" v-if="migrating.process"></i> Migrate
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" :disabled="migrating.process">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>