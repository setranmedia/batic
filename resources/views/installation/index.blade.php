@extends('layouts.install')

@section('content')
<div id="installation">
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			@if($status)
				@include('installation.token')
				@include('installation.migrate')
				@include('installation.complete')
			@else
				@include('installation.requirements')
			@endif
		</div>
	</div>
</div>
@endsection

@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#installation',
			data: {
				btracker: {
					process: false,
					email: '',
					password: '',
					status: {!! json_encode($status['btracker']) !!}
				},
				admin: {
					name: {!! json_encode(config('batic.defaultAdmin.name')) !!},
					email: {!! json_encode(config('batic.defaultAdmin.email')) !!},
					password: '',
					repassword: ''
				},
				database: {!! json_encode($database) !!},
				migrating: {
					process: false,
					submited: false,
					success: false,
					message: '',
					class: 'alert alert-danger'
				}
			},
			methods: {
				getToken: function(e){
					e.preventDefault();
					this.btracker.process=true;
					var post = {
						email: this.btracker.email,
						password: this.btracker.password
					};
					this.$http.post({!! json_encode(route('installation.getToken')) !!},post).then(function(r){
						var response = r.data;
						if(response.status){
							if(response.status == 'success'){
								$.toaster('Token saved successfully', 'Success', 'success');
								this.btracker.status = true;
								window.location.reload();
							}
							else if(response.status == 401)
								$.toaster('invalid_credentials', 'Failed', 'warning');
							else if(response.status == 500)
								$.toaster('BTracker.co error, Please contact Pijar', 'Failed', 'warning');
						}else{
							$.toaster('Failed to get token', 'Failed', 'warning');
						}
						this.btracker.process=false;
					},function(r){
						$.toaster('Fatal Error', "Error", 'danger');
						this.btracker.process = false;
					});
				},
				migrateDatabase: function(e){
					e.preventDefault();
					this.migrating.process = true;
					this.$http.post({!! json_encode(route('installation.migrateDatabase')) !!},this.admin).then(function(r){
						var response = r.data;
						if(response.status == 'success'){
							window.location.reload();
						}else{
							$.toaster(response.message, 'Error', 'danger');
						}		
					},function(r){
						$.toaster('Fatal Error', "Error", 'danger');
					});
				}
			}
		});
	});
</script>
@endsection