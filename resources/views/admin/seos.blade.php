@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>SEO Rules</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<div class="pull-right">
							<button class="btn btn-info" data-toggle="modal" data-target="#addNew">Add New</button>
						</div>
						<form class="form-inline"s return="false">
							<div class="form-group">
								<select id="parent-id" class="form-control" v-model="seoType" v-on:change="refreshSeoType">
									<option value="home">Home</option>
									<option value="category">Category</option>
									<option value="keyword">Keyword</option>
									<option value="search">Search</option>
									<option value="page">Manual Page</option>
								</select>
							</div>
						</form>
					</div>
					<div class="box-body">
						<table id="datatables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#ID</th>
									<th>Name</th>
									<th>Meta Robot</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>
	<div id="addNew" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="create">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">New SEO Rule</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="new-seo_id">Type</label>
							<div class="col-md-9">
								<select id="new-seo_id" class="form-control" v-model="seoType" v-on:change="refreshSeoType">
									<option value="home">Home</option>
									<option value="category">Category</option>
									<option value="keyword">Keyword</option>
									<option value="search">Search</option>
									<option value="page">Manual Page</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-name">Rule Name</label>
							<div class="col-md-9">
								<input type="text" id="default-name" class="form-control" v-model="default.name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-meta_title">Meta Title</label>
							<div class="col-md-9">
								<input type="text" id="default-meta_title" class="form-control" v-model="default.meta_title">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-meta_keyword">Meta Keyword</label>
							<div class="col-md-9">
								<input type="text" id="default-meta_keyword" class="form-control" v-model="default.meta_keyword">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-meta_robot">Meta Robot</label>
							<div class="col-md-9">
								<input type="text" id="default-meta_robot" class="form-control" v-model="default.meta_robot">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-description">Meta Desc</label>
							<div class="col-md-9">
								<textarea id="default-description" class="form-control" v-model="default.meta_description" placeholder="Some description here..."></textarea>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Creating...</span>
							<span v-else>Create</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="editData" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="update">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Edit SEO Rule</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="current-type">Type</label>
							<div class="col-md-9">
								<select id="current-type" class="form-control" v-model="current.type">
									<option value="home">Home</option>
									<option value="category">Category</option>
									<option value="keyword">Keyword</option>
									<option value="search">Search</option>
									<option value="page">Manual Page</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-name">Rule Name</label>
							<div class="col-md-9">
								<input type="text" id="current-name" class="form-control" v-model="current.name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-meta_title">Meta Title</label>
							<div class="col-md-9">
								<input type="text" id="current-meta_title" class="form-control" v-model="current.meta_title">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-meta_keyword">Meta Keyword</label>
							<div class="col-md-9">
								<input type="text" id="current-meta_keyword" class="form-control" v-model="current.meta_keyword">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-meta_robot">Meta Robot</label>
							<div class="col-md-9">
								<input type="text" id="current-meta_robot" class="form-control" v-model="current.meta_robot">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-description">Meta Desc</label>
							<div class="col-md-9">
								<textarea id="current-description" class="form-control" v-model="current.meta_description" placeholder="Some description here..."></textarea>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Updating...</span>
							<span v-else>Update</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="deleteData" class="modal modal-danger fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="delete">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Delete Seo Rule</h4>
					</div>

					<div class="modal-body">
						<p>Are you sure, want to delete this SEO Rule <strong>@{{current.name}}</strong>?</p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Deleting...</span>
							<span v-else>Delete</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				dataTable: {},
				current: {},
				default: {},
				seoType: 'home',
				seos: {},
				process: false,
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getDefault();
				this.getDataTable();
			},
			methods: {
				getDefault: function(){
					this.$set('process',true);
					var seoType = this.seoType;
					this.$http.post({!! json_encode(route('admin.ajax.get.config')) !!},{name:'batic.defaultSeo'}).then(function(r){
						this.$set('seos',r.data);
						this.$set('default',r.data[seoType]);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getDataTable: function(){
					var vueThis = this;
					this.dataTable = $('#datatables').DataTable({
						processing: true,
						serverSide: true,
						ajax: {
							url: {!! json_encode(route('admin.ajax.datatable.seos')) !!},
							type: 'POST',
							data: function (d) {
								d.type = vueThis.seoType;
							},
							headers: { 'X-CSRF-TOKEN': vueThis.csrfToken }
						},
						columns: [
							{ data: 'id', name: 'id' },
							{ data: 'name', name: 'name' },
							{ data: 'meta_robot', name: 'meta_robot' },
							{
								"className": 'text-center',
								"orderable": false,
								"searchable": false,
								"data": null,
								"defaultContent": '<button type="button" class="btn btn-xs btn-success edit-data" title="Edit"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-xs btn-danger delete-data" title="Delete"><i class="fa fa-trash-o"></i></button>'
							}
						]
					});
				},
				refreshSeoType: function(){
					var seos = this.seos;
					var seoType = this.seoType;
					this.$set('default',seos[seoType]);
					this.dataTable.ajax.reload();
				},
				create: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.createSeo')) !!},{seo: this.default}).then(function(r){
						this.$set('process',false);
						toastr.success('New SEO Rule Created', 'Success');
						$('#addNew').modal('toggle');
						this.refreshSeoType();
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				update: function(e){
					e.preventDefault();
					this.$set('process',1);
					var post = {
						seo_id: this.current.id,
						seo: {
							name: this.current.name,
							meta_title: this.current.meta_title,
							meta_keyword: this.current.meta_keyword,
							meta_description: this.current.meta_description,
							type: this.current.type,
							meta_robot: this.current.meta_robot,
						}
					};
					this.$http.post({!! json_encode(route('admin.ajax.post.updateSeo')) !!},post).then(function(r){
						this.$set('process',false);
						toastr.success('SEO Rule Updated', 'Success');
						$('#editData').modal('toggle');
						this.seoType = this.current.type;
						this.refreshSeoType();
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				delete: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.deleteSeo')) !!},{seo_id: this.current.id}).then(function(r){
						this.$set('process',false);
						toastr.success('SEO Rule Deleted', 'Success');
						this.refreshSeoType();
						$('#deleteData').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
			}
		});
		@include('admin.partials.js.CheckAlive')

		$('#datatables tbody').on('click', 'td button.edit-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			var current = rowStats.data();
			vue.$set('current',current);
			$("#editData").modal();
		});

		$('#datatables tbody').on('click', 'td button.delete-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			vue.$set('current',rowStats.data());
			$("#deleteData").modal();
		});

	});
</script>
@endsection