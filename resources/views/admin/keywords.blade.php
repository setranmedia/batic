@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Keywords</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<form class="form-inline"s return="false">
							<div class="form-group">
								<select id="parent-id" class="form-control" v-model="type" v-on:change="refreshKeywords">
									<option value="products">Products</option>
									<option value="google_images">Google Images</option>
								</select>
							</div>
						</form>
					</div>
					<div class="box-body">
						<table id="datatables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#ID</th>
									<th>Query</th>
									<th>Type</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>

				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Queues</h3>
						<div class="pull-right">
							<button class="btn btn-info" data-toggle="modal" data-target="#addNew">Add New</button>
						</div>
					</div>
					<div class="box-body">
						<table id="queuestables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#ID</th>
									<th>Total</th>
									<th>Executed</th>
									<th>Success</th>
									<th>Failed</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>
	<div id="addNew" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="createQueue">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">New Queue</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label" for="default-keywords"></label>
							<div class="col-md-12">
								<textarea id="default-keywords" class="form-control" v-model="keywords" rows="10" placeholder="Separate each keyword by new line..." required></textarea>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Adding to Queue...</span>
							<span v-else>Add to Queue</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="deleteKeyword" class="modal modal-danger fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="deleteKeyword">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Delete Keyword</h4>
					</div>

					<div class="modal-body">
						<p>Are you sure, want to delete this keyword <strong class="bg-yellow">@{{current.query}}</strong>?</p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Deleting...</span>
							<span v-else>Delete</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="deleteQueue" class="modal modal-danger fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="deleteQueue">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Delete Template</h4>
					</div>

					<div class="modal-body">
						<p>Are you sure, want to delete Queue <strong class="bg-yellow">#ID: @{{current.id}}</strong>?</p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Deleting...</span>
							<span v-else>Delete</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				dataTable: {},
				queueTable: {},
				current: {},
				keywords: '',
				type: 'products',
				process: false,
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getDataTable();
				this.getQueueTable();
			},
			methods: {
				getDataTable: function(){
					var vueThis = this;
					this.dataTable = $('#datatables').DataTable({
						processing: true,
						serverSide: true,
						ajax: {
							url: {!! json_encode(route('admin.ajax.datatable.keywords')) !!},
							type: 'POST',
							data: function (d) {
								d.type = vueThis.type;
							},
							headers: { 'X-CSRF-TOKEN': vueThis.csrfToken }
						},
						columns: [
							{ data: 'id', name: 'id' },
							{ data: 'query', name: 'query' },
							{ data: 'type', name: 'type' },
							{
								"className": 'text-center',
								"orderable": false,
								"searchable": false,
								"data": null,
								"defaultContent": '<button type="button" class="btn btn-xs btn-danger delete-data" title="Delete"><i class="fa fa-trash-o"></i></button> <button type="button" class="btn btn-xs btn-primary open-data" title="View"><i class="fa fa-external-link"></i></button>'
							}
						]
					});
				},
				getQueueTable: function(){
					var vueThis = this;
					this.queueTable = $('#queuestables').DataTable({
						processing: true,
						serverSide: true,
						ajax: {
							url: {!! json_encode(route('admin.ajax.datatable.queues')) !!},
							type: 'POST',
							headers: { 'X-CSRF-TOKEN': vueThis.csrfToken }
						},
						columns: [
							{ data: 'id', name: 'id' },
							{ data: 'total', name: 'total' },
							{ data: 'executed', name: 'executed' },
							{ data: 'success', name: 'success' },
							{ data: 'failed', name: 'failed' },
							{
								"className": 'text-center',
								"orderable": false,
								"searchable": false,
								"data": null,
								"defaultContent": '<button type="button" class="btn btn-xs btn-danger delete-data" title="Delete"><i class="fa fa-trash-o"></i></button>'
							}
						]
					});
				},
				refreshKeywords: function(){
					this.dataTable.ajax.reload();
				},
				refreshQueues: function(){
					this.keywords = "";
					this.queueTable.ajax.reload();
				},
				createQueue: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.createQueue')) !!},{keywords: this.keywords}).then(function(r){
						this.$set('process',false);
						toastr.success('New Queue Created', 'Success');
						$('#addNew').modal('toggle');
						this.refreshQueues();
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				deleteQueue: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.deleteQueue')) !!},{queue_id: this.current.id}).then(function(r){
						this.$set('process',false);
						toastr.success('Queue Deleted', 'Success');
						this.refreshQueues();
						$('#deleteQueue').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				deleteKeyword: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.deleteKeyword')) !!},{keyword_id: this.current.id}).then(function(r){
						this.$set('process',false);
						toastr.success('Keyword Deleted', 'Success');
						this.refreshKeywords();
						$('#deleteKeyword').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
			}
		});
		@include('admin.partials.js.CheckAlive')

		$('#datatables tbody').on('click', 'td button.open-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			var keyword = rowStats.data();
			window.open(keyword.url, '_blank');
		});

		$('#datatables tbody').on('click', 'td button.delete-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			vue.$set('current',rowStats.data());
			$("#deleteKeyword").modal();
		});

		$('#queuestables tbody').on('click', 'td button.delete-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.queueTable.row( trStats );
			vue.$set('current',rowStats.data());
			$("#deleteQueue").modal();
		});

	});
</script>
@endsection