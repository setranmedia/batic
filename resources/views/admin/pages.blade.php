@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Manual Pages</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<div class="pull-right">
							<button class="btn btn-info" data-toggle="modal" data-target="#addNew">Add New</button>
						</div>
					</div>
					<div class="box-body">
						<table id="datatables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#ID</th>
									<th>Name</th>
									<th>Updated</th>
									<th>Created</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>
	<div id="addNew" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="create">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">New Page</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="new-name">Name/Title</label>
							<div class="col-md-9">
								<input type="text" id="new-name" class="form-control" v-model="default.name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-content">Content</label>
							<div class="col-md-9">
								<textarea id="new-content" class="form-control" v-model="default.content" placeholder="Place content here..." rows="10"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-seo_id">Meta SEO Rule</label>
							<div class="col-md-9">
								<select id="new-seo_id" class="form-control" v-model="default.seo_id">
									<option :value="false">Create New Rule</option>
									<option v-for="seo in seos" :value="seo.id">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div v-if="!default.seo_id">
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-name">Rule Name</label>
								<div class="col-md-9">
									<input type="text" id="newSeo-name" class="form-control" v-model="newSeo.name" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-meta_title">Meta Title</label>
								<div class="col-md-9">
									<input type="text" id="newSeo-meta_title" class="form-control" v-model="newSeo.meta_title">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-meta_keyword">Meta Keyword</label>
								<div class="col-md-9">
									<input type="text" id="newSeo-meta_keyword" class="form-control" v-model="newSeo.meta_keyword">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-meta_robot">Meta Robot</label>
								<div class="col-md-9">
									<input type="text" id="newSeo-meta_robot" class="form-control" v-model="newSeo.meta_robot">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-description">Meta Desc</label>
								<div class="col-md-9">
									<textarea id="newSeo-description" class="form-control" v-model="newSeo.meta_description" placeholder="Some description here..."></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Creating...</span>
							<span v-else>Create</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="editData" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="update">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Edit Page</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="current-name">Name/Title</label>
							<div class="col-md-9">
								<input type="text" id="current-name" class="form-control" v-model="current.name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-content">Content</label>
							<div class="col-md-9">
								<textarea id="current-content" class="form-control" v-model="current.content" placeholder="Place content here..." rows="10"></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3" for="current-seo_id">Meta SEO Rule</label>
							<div class="col-md-9">
								<select id="current-seo_id" class="form-control" v-model="current.seo_id">
									<option :value="false">Create New Rule</option>
									<option v-for="seo in seos" :value="seo.id">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div v-if="!current.seo_id">
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-name">Rule Name</label>
								<div class="col-md-9">
									<input type="text" id="newSeo-name" class="form-control" v-model="newSeo.name" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-meta_title">Meta Title</label>
								<div class="col-md-9">
									<input type="text" id="newSeo-meta_title" class="form-control" v-model="newSeo.meta_title">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-meta_keyword">Meta Keyword</label>
								<div class="col-md-9">
									<input type="text" id="newSeo-meta_keyword" class="form-control" v-model="newSeo.meta_keyword">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-meta_robot">Meta Robot</label>
								<div class="col-md-9">
									<input type="text" id="newSeo-meta_robot" class="form-control" v-model="newSeo.meta_robot">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3" for="newSeo-description">Meta Desc</label>
								<div class="col-md-9">
									<textarea id="newSeo-description" class="form-control" v-model="newSeo.meta_description" placeholder="Some description here..."></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Updating...</span>
							<span v-else>Update</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="deleteData" class="modal modal-danger fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="delete">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Delete Page</h4>
					</div>

					<div class="modal-body">
						<p>Are you sure, want to delete this page <strong>@{{current.name}}</strong>?</p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Deleting...</span>
							<span v-else>Delete</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				dataTable: {},
				productsTable: false,
				current: {},
				default: {},
				seos: {},
				newSeo: {},
				process: false,
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getDefault();
				this.getDefaultSeo();
				this.getDataTable();
				this.getSeos();
			},
			methods: {
				getDefault: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.config')) !!},{name:'batic.defaultPage'}).then(function(r){
						this.$set('default',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getDefaultSeo: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.config')) !!},{name:'batic.defaultSeo.page'}).then(function(r){
						this.$set('newSeo',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getSeos: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.seos')) !!},{type:'page'}).then(function(r){
						this.$set('seos',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getDataTable: function(){
					var vueThis = this;
					this.dataTable = $('#datatables').DataTable({
						processing: true,
						serverSide: true,
						ajax: {
							url: {!! json_encode(route('admin.ajax.datatable.pages')) !!},
							type: 'POST',
							headers: { 'X-CSRF-TOKEN': vueThis.csrfToken }
						},
						columns: [
							{ data: 'id', name: 'id' },
							{ data: 'name', name: 'name' },
							{ data: 'updated_at', name: 'updated_at', searchable: false },
							{ data: 'created_at', name: 'created_at', searchable: false },
							{
								"className": 'text-center',
								"orderable": false,
								"searchable": false,
								"data": null,
								"defaultContent": '<button type="button" class="btn btn-xs btn-success edit-data" title="Edit"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-xs btn-danger delete-data" title="Delete"><i class="fa fa-trash-o"></i></button> <button type="button" class="btn btn-xs btn-primary open-data" title="View"><i class="fa fa-external-link"></i></button>'
							}
						]
					});
				},
				refreshDataTable: function(){
					this.dataTable.ajax.reload();
				},
				create: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.createPage')) !!},{page: this.default,seo:this.newSeo}).then(function(r){
						this.$set('process',false);
						toastr.success('New Manual Page Created', 'Success');
						this.refreshDataTable();
						if(!this.default.seo_id){
							this.getSeos();
							this.getDefaultSeo();
						}
						$('#addNew').modal('toggle');
						this.getDefault();
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				update: function(e){
					e.preventDefault();
					this.$set('process',1);
					var post = {
						page_id: this.current.id,
						page: {
							name: this.current.name,
							content: this.current.content,
							seo_id: this.current.seo_id,
						},
						seo: this.newSeo
					};
					this.$http.post({!! json_encode(route('admin.ajax.post.updatePage')) !!},post).then(function(r){
						this.$set('process',false);
						toastr.success('Manual Page Updated', 'Success');
						this.refreshDataTable();
						if(!this.current.seo_id){
							this.getSeos();
							this.getDefaultSeo();
						}
						$('#editData').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				delete: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.deletePage')) !!},{page_id: this.current.id}).then(function(r){
						this.$set('process',false);
						toastr.success('Manual Page Deleted', 'Success');
						this.refreshDataTable();
						$('#deleteData').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
			}
		});
		@include('admin.partials.js.CheckAlive')

		$('#datatables tbody').on('click', 'td button.edit-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			var current = rowStats.data();
			current.parent_id = current.parent_id == null ?'':current.parent_id;
			vue.$set('current',current);
			$("#editData").modal();
		});

		$('#datatables tbody').on('click', 'td button.delete-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			vue.$set('current',rowStats.data());
			$("#deleteData").modal();
		});

		$('#datatables tbody').on('click', 'td button.open-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			var page = rowStats.data();
			window.open(page.url, '_blank');
		});

	});
</script>
@endsection