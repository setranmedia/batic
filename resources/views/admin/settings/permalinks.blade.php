@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Permalinks Setting</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<form method="POST" class="form-horizontal" v-on:submit="updatePermalinks">
						<div class="box-body">
							<div class="form-group">
								<label for="permalinks-category" class="col-sm-2 control-label">Category</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="permalinks-category" v-model="permalinks.category" required>
								</div>
							</div>
							<div class="form-group">
								<label for="permalinks-page" class="col-sm-2 control-label">Manual Page</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="permalinks-page" v-model="permalinks.page" required>
								</div>
							</div>
							<div class="form-group">
								<label for="permalinks-keyword" class="col-sm-2 control-label">Keyword</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="permalinks-keyword" v-model="permalinks.keyword" required>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-info pull-right">Save changes</button>
						</div>
					</form>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				permalinks: {},
				process: 0,
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getPermalinks();
			},
			methods: {
				getPermalinks: function(){
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.get.config')) !!},{name:'batic.permalinks'}).then(function(r){
						this.$set('permalinks',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				updatePermalinks: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.updateBatic')) !!},{name:'permalinks',values:this.permalinks}).then(function(r){
						this.$set('process',false);
						toastr.success('Permalinks Updated', 'Success');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
			}
		});
		@include('admin.partials.js.CheckAlive')
	});
</script>
@endsection