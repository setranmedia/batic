@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Sitemaps Setting</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<form method="POST" class="form-horizontal" v-on:submit="updatePermalinks">
						<div class="box-body">
							<div class="form-group">
								<label for="sitemaps-url_per_file" class="col-sm-2 control-label">URL per Page</label>
								<div class="col-sm-10">
									<input type="number" class="form-control" id="sitemaps-url_per_file" v-model="sitemaps.url_per_file" required>
								</div>
							</div>
							<div class="form-group">
								<label for="sitemaps-main_sitemap" class="col-sm-2 control-label">Main Sitemap</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="sitemaps-main_sitemap" v-model="sitemaps.main_sitemap" required>
								</div>
							</div>
							<div class="form-group">
								<label for="sitemaps-catalog_sitemap" class="col-sm-2 control-label">Catalog Sitemap</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="sitemaps-catalog_sitemap" v-model="sitemaps.catalog_sitemap" required>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-info pull-right">Save changes</button>
						</div>
					</form>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				sitemaps: {},
				process: 0,
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getPermalinks();
			},
			methods: {
				getPermalinks: function(){
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.get.config')) !!},{name:'batic.sitemaps'}).then(function(r){
						this.$set('sitemaps',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				updatePermalinks: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.updateBatic')) !!},{name:'sitemaps',values:this.sitemaps}).then(function(r){
						this.$set('process',false);
						toastr.success('Sitemaps Updated', 'Success');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
			}
		});
		@include('admin.partials.js.CheckAlive')
	});
</script>
@endsection