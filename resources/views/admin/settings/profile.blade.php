@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Admin Profile</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<form method="POST" class="form-horizontal" v-on:submit="updateProfile">
						<div class="box-body">
							<div class="form-group">
								<label for="profile-name" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="profile-name" v-model="profile.name" required>
								</div>
							</div>
							<div class="form-group">
								<label for="profile-email" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" id="profile-email" v-model="profile.email" required>
								</div>
							</div>
							<div class="form-group">
								<label for="profile-password" class="col-sm-2 control-label">Password</label>
								<div class="col-sm-10">
									<button class="btn btn-warning btn-sm" type="button" data-toggle="modal" data-target="#updatePassword">Change Password</button>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-info pull-right">Save changes</button>
						</div>
					</form>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>

	<div id="updatePassword" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="updatePassword">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Change Password</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="profile-password">Current Password</label>
							<div class="col-md-9">
								<input type="password" id="profile-password" class="form-control" required="" v-model="password.old">
							</div>
						</div>
						<div class="form-group @{{ password.new.length < 6 ? 'has-error' : ''}}">
							<label class="control-label col-md-3" for="profile-new-password">New Password</label>
							<div class="col-md-9">
								<input type="password" id="profile-new-password" class="form-control" required="" v-model="password.new">
								<span class="help-block" v-if="password.new.length < 6">Minimal 6 karakter</span>
							</div>
						</div>
						<div class="form-group @{{ password.renew != password.new ? 'has-error' : ''}}">
							<label class="control-label col-md-3" for="profile-retype-password">Retype New Password</label>
							<div class="col-md-9">
								<input type="password" id="profile-retype-password" class="form-control" required="" v-model="password.renew">
								<span class="help-block" v-if="password.renew != password.new">Password tidak sama</span>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Changing Password...</span>
							<span v-else>Change Password</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				profile: {},
				password: {
					old: '',
					new: '',
					renew: '',
				},
				process: 0,
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getProfile();
			},
			methods: {
				getProfile: function(){
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.get.profile')) !!}).then(function(r){
						this.$set('profile',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				updateProfile: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.updateProfile')) !!},this.profile).then(function(r){
						this.$set('process',false);
						toastr.success('Profile Updated', 'Success');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				updatePassword: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.updatePassword')) !!},this.password).then(function(r){
						this.$set('process',false);
						toastr.success('Password Updated', 'Success');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
			}
		});
		@include('admin.partials.js.CheckAlive')
	});
</script>
@endsection