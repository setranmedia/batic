<div id="loginDialog" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal form-bordered" method="POST" action="{!! route('admin.authenticate') !!}">
				<div class="modal-header">
					<h4 class="modal-title">Sign In <small class="bg-yellow">Your session has expired.</small></h4>
				</div>
				<div class="modal-body">
					<input type="hidden" name="_token" v-model="csrfToken">
					<div class="form-group">
						<label class="control-label col-md-3" for="login-email">Email</label>
						<div class="col-md-9">
							<input type="email" name="email" id="login-email" class="form-control" required="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3" for="login-password">Password</label>
						<div class="col-md-9">
							<input type="password" name="password" id="login-password" class="form-control" required="">
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-outline">Sign In</button>
				</div>
			</form>
		</div>
	</div>
</div>