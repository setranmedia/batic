function refreshCsrfToken(){
	setTimeout(function() {
		getCsrfToken();
		setCsrfToken();
		refreshCsrfToken();
	},(5 * 60 * 1000));
}
function getCsrfToken(){
	vue.$http.get({!! json_encode(route('public.ajax.get.checkAlive')) !!}).then(function(r){
		vue.$set('csrfToken',r.data.csrf_token);
		$('meta[name="csrf-token"]').attr('content',r.data.csrf_token);
		if(! r.data.logged_in){
			$("#loginDialog").modal();
		}
	});
}
function setCsrfToken(){
	Vue.http.headers.common['X-CSRF-TOKEN'] = vue.csrfToken;
}
refreshCsrfToken();