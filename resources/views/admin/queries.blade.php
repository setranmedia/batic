@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Keywords</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<form class="form-inline"s return="false">
							<div class="form-group">
								<select id="parent-id" class="form-control" v-model="type" v-on:change="refreshKeywords">
									<option value="search-engine">Search Engine</option>
									<option value="search-form">Search Form</option>
								</select>
							</div>
						</form>
					</div>
					<div class="box-body">
						<table id="datatables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#ID</th>
									<th>Query</th>
									<th>Type</th>
									<th>Total</th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				dataTable: {},
				type: 'search-engine',
				process: false,
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getDataTable();
				this.getQueueTable();
			},
			methods: {
				getDataTable: function(){
					var vueThis = this;
					this.dataTable = $('#datatables').DataTable({
						processing: true,
						serverSide: true,
						ajax: {
							url: {!! json_encode(route('admin.ajax.datatable.queries')) !!},
							type: 'POST',
							data: function (d) {
								d.type = vueThis.type;
							},
							headers: { 'X-CSRF-TOKEN': vueThis.csrfToken }
						},
						columns: [
							{ data: 'id', name: 'id' },
							{ data: 'query', name: 'query' },
							{ data: 'type', name: 'type' },
							{ data: 'total', name: 'total' }
						]
					});
				},
				refreshKeywords: function(){
					this.dataTable.ajax.reload();
				},
			}
		});
		@include('admin.partials.js.CheckAlive')

	});
</script>
@endsection