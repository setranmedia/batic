@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Domains</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<div class="pull-right">
							<button class="btn btn-info" data-toggle="modal" data-target="#addNew">Add New</button>
						</div>
					</div>
					<div class="box-body">
						<table id="datatables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#ID</th>
									<th>Domain</th>
									<th>Site Name</th>
									<th>Theme</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>
	<div id="addNew" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="create">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">New Domain</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="new-domain">Domain</label>
							<div class="col-md-9">
								<input type="text" id="new-domain" class="form-control" v-model="default.domain" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-ssl">SSL</label>
							<div class="col-md-9">
								<select id="new-ssl" class="form-control" v-model="default.ssl" required>
									<option :value="1">Yes</option>
									<option :value="0">No</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-site_name">Site Name</label>
							<div class="col-md-9">
								<input type="text" id="new-site_name" class="form-control" v-model="default.site_name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-per_page">Products per Page</label>
							<div class="col-md-9">
								<input type="number" id="new-per_page" class="form-control" v-model="default.per_page" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-theme">Theme</label>
							<div class="col-md-9">
								<select id="default-theme" class="form-control" v-model="default.theme" required>
									<option v-for="theme in themes" :value="theme">@{{theme}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="default.categories" class="col-md-3 control-label">Categories</label>
							<div class="col-md-9">
								<select class="form-control" id="default.categories" v-model="default.categories" multiple="multiple">
									<option v-for="category in categories" :value="category.id">@{{ category.name }}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="default.pages" class="col-md-3 control-label">Pages</label>
							<div class="col-md-9">
								<select class="form-control" id="default.pages" v-model="default.pages" multiple="multiple">
									<option v-for="page in pages" :value="page.id">@{{ page.name }}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-header_script">Header Script</label>
							<div class="col-md-9">
								<textarea id="new-header_script" class="form-control" v-model="default.header_script" placeholder="Place header script, like google web master meta here..." rows="5"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-footer_script">Header Script</label>
							<div class="col-md-9">
								<textarea id="new-footer_script" class="form-control" v-model="default.footer_script" placeholder="Place footer script, like histats code here..." rows="5"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-seo_home_id">SEO Home Rule</label>
							<div class="col-md-9">
								<select id="default-seo_home_id" class="form-control" v-model="default.seo_home_id" required>
									<option v-for="seo in seos" :value="seo.id" v-if="seo.type == 'home'">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-seo_category_id">SEO Category Rule</label>
							<div class="col-md-9">
								<select id="default-seo_category_id" class="form-control" v-model="default.seo_category_id" required>
									<option v-for="seo in seos" :value="seo.id" v-if="seo.type == 'category'">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-seo_keyword_id">SEO Keyword Rule</label>
							<div class="col-md-9">
								<select id="default-seo_keyword_id" class="form-control" v-model="default.seo_keyword_id" required>
									<option v-for="seo in seos" :value="seo.id" v-if="seo.type == 'keyword'">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-seo_search_id">SEO Search Rule</label>
							<div class="col-md-9">
								<select id="default-seo_search_id" class="form-control" v-model="default.seo_search_id" required>
									<option v-for="seo in seos" :value="seo.id" v-if="seo.type == 'search'">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-template_home_id">Home Template</label>
							<div class="col-md-9">
								<select id="default-template_home_id" class="form-control" v-model="default.template_home_id" required>
									<option v-for="template in templates" :value="template.id" v-if="template.type == 'home'">#@{{template.id}} @{{template.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-template_category_id">Category Template</label>
							<div class="col-md-9">
								<select id="default-template_category_id" class="form-control" v-model="default.template_category_id" required>
									<option v-for="template in templates" :value="template.id" v-if="template.type == 'category'">#@{{template.id}} @{{template.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-template_keyword_id">Keyword Template</label>
							<div class="col-md-9">
								<select id="default-template_keyword_id" class="form-control" v-model="default.template_keyword_id" required>
									<option v-for="template in templates" :value="template.id" v-if="template.type == 'keyword'">#@{{template.id}} @{{template.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="default-template_search_id">Search Template</label>
							<div class="col-md-9">
								<select id="default-template_search_id" class="form-control" v-model="default.template_search_id" required>
									<option v-for="template in templates" :value="template.id" v-if="template.type == 'search'">#@{{template.id}} @{{template.name}}</option>
								</select>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Creating...</span>
							<span v-else>Create</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="editData" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="update">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Edit Domain</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="current-domain">Domain</label>
							<div class="col-md-9">
								<input type="text" id="current-domain" class="form-control" v-model="current.domain" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-ssl">SSL</label>
							<div class="col-md-9">
								<select id="current-ssl" class="form-control" v-model="current.ssl" required>
									<option :value="1">Yes</option>
									<option :value="0">No</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-site_name">Site Name</label>
							<div class="col-md-9">
								<input type="text" id="current-site_name" class="form-control" v-model="current.site_name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-per_page">Products per Page</label>
							<div class="col-md-9">
								<input type="number" id="current-per_page" class="form-control" v-model="current.per_page" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-theme">Theme</label>
							<div class="col-md-9">
								<select id="current-theme" class="form-control" v-model="current.theme" required>
									<option v-for="theme in themes" :value="theme">@{{theme}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="current.categories" class="col-md-3 control-label">Categories</label>
							<div class="col-md-9">
								<select class="form-control" id="current.categories" v-model="current.categories" multiple="multiple">
									<option v-for="category in categories" :value="category.id">@{{ category.name }}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="current.pages" class="col-md-3 control-label">Pages</label>
							<div class="col-md-9">
								<select class="form-control" id="current.pages" v-model="current.pages" multiple="multiple">
									<option v-for="page in pages" :value="page.id">@{{ page.name }}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-header_script">Header Script</label>
							<div class="col-md-9">
								<textarea id="current-header_script" class="form-control" v-model="current.header_script" placeholder="Place header script, like google web master meta here..." rows="5"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-footer_script">Footer Script</label>
							<div class="col-md-9">
								<textarea id="current-footer_script" class="form-control" v-model="current.footer_script" placeholder="Place footer script, like histats code here..." rows="5"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-seo_home_id">SEO Home Rule</label>
							<div class="col-md-9">
								<select id="current-seo_home_id" class="form-control" v-model="current.seo_home_id" required>
									<option v-for="seo in seos" :value="seo.id" v-if="seo.type == 'home'">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-seo_category_id">SEO Category Rule</label>
							<div class="col-md-9">
								<select id="current-seo_category_id" class="form-control" v-model="current.seo_category_id" required>
									<option v-for="seo in seos" :value="seo.id" v-if="seo.type == 'category'">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-seo_keyword_id">SEO Keyword Rule</label>
							<div class="col-md-9">
								<select id="current-seo_keyword_id" class="form-control" v-model="current.seo_keyword_id" required>
									<option v-for="seo in seos" :value="seo.id" v-if="seo.type == 'keyword'">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-seo_search_id">SEO Search Rule</label>
							<div class="col-md-9">
								<select id="current-seo_search_id" class="form-control" v-model="current.seo_search_id" required>
									<option v-for="seo in seos" :value="seo.id" v-if="seo.type == 'search'">#@{{seo.id}} @{{seo.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-template_home_id">Home Template</label>
							<div class="col-md-9">
								<select id="current-template_home_id" class="form-control" v-model="current.template_home_id" required>
									<option v-for="template in templates" :value="template.id" v-if="template.type == 'home'">#@{{template.id}} @{{template.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-template_category_id">Category Template</label>
							<div class="col-md-9">
								<select id="current-template_category_id" class="form-control" v-model="current.template_category_id" required>
									<option v-for="template in templates" :value="template.id" v-if="template.type == 'category'">#@{{template.id}} @{{template.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-template_keyword_id">Keyword Template</label>
							<div class="col-md-9">
								<select id="current-template_keyword_id" class="form-control" v-model="current.template_keyword_id" required>
									<option v-for="template in templates" :value="template.id" v-if="template.type == 'keyword'">#@{{template.id}} @{{template.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-template_search_id">Search Template</label>
							<div class="col-md-9">
								<select id="current-template_search_id" class="form-control" v-model="current.template_search_id" required>
									<option v-for="template in templates" :value="template.id" v-if="template.type == 'search'">#@{{template.id}} @{{template.name}}</option>
								</select>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Updating...</span>
							<span v-else>Update</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="deleteData" class="modal modal-danger fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="delete">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Delete Domain</h4>
					</div>

					<div class="modal-body">
						<p>Are you sure, want to delete this domain <strong class="bg-yellow">@{{current.domain}}</strong>?</p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Deleting...</span>
							<span v-else>Delete</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="flushData" class="modal modal-warning fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="flush">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Flush Domain</h4>
					</div>

					<div class="modal-body">
						<p>This action will deleting all stored cache data related this domain: <strong class="bg-yellow">@{{current.domain}}</strong> and try to refreshing data.</p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Flushing...</span>
							<span v-else>Flush</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				dataTable: {},
				productsTable: false,
				current: {},
				default: {},
				seos: {},
				templates: {},
				categories: {},
				pages: {},
				themes: {},
				newSeos: {},
				process: false,
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getDefault();
				this.getDefaultSeo();
				this.getDataTable();
				this.getSeos();
				this.getTemplates();
				this.getCategories();
				this.getPages();
				this.getThemes();
			},
			methods: {
				getDefault: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.config')) !!},{name:'batic.defaultDomain'}).then(function(r){
						this.$set('default',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getDefaultSeo: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.config')) !!},{name:'batic.defaultSeo'}).then(function(r){
						this.$set('newSeos',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getSeos: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.seos')) !!}).then(function(r){
						this.$set('seos',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getTemplates: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.templates')) !!}).then(function(r){
						this.$set('templates',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getCategories: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.categories')) !!}).then(function(r){
						this.$set('categories',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getPages: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.pages')) !!}).then(function(r){
						this.$set('pages',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getThemes: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.themes')) !!}).then(function(r){
						this.$set('themes',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getDataTable: function(){
					var vueThis = this;
					this.dataTable = $('#datatables').DataTable({
						processing: true,
						serverSide: true,
						ajax: {
							url: {!! json_encode(route('admin.ajax.datatable.domains')) !!},
							type: 'POST',
							headers: { 'X-CSRF-TOKEN': vueThis.csrfToken }
						},
						columns: [
							{ data: 'id', name: 'id' },
							{ data: 'domain', name: 'domain' },
							{ data: 'site_name', name: 'site_name' },
							{ data: 'theme', name: 'theme' },
							{
								"className": 'text-center',
								"orderable": false,
								"searchable": false,
								"data": null,
								"defaultContent": '<button type="button" class="btn btn-xs btn-success edit-data" title="Edit"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-xs btn-warning flush-data" title="Flush and Refresh Cache"><i class="fa fa-refresh"></i></button> <button type="button" class="btn btn-xs btn-danger delete-data" title="Delete"><i class="fa fa-trash-o"></i></button> <button type="button" class="btn btn-xs btn-primary open-data" title="View"><i class="fa fa-external-link"></i></button>'
							}
						]
					});
				},
				refreshDataTable: function(){
					this.dataTable.ajax.reload();
				},
				create: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.createDomain')) !!},{domain: this.default}).then(function(r){
						this.$set('process',false);
						toastr.success('New Domain Created', 'Success');
						this.refreshDataTable();
						$('#addNew').modal('toggle');
						this.getDefault();
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				update: function(e){
					e.preventDefault();
					this.$set('process',1);
					var post = {
						domain_id: this.current.id,
						domain: this.current
					};
					this.$http.post({!! json_encode(route('admin.ajax.post.updateDomain')) !!},post).then(function(r){
						this.$set('process',false);
						toastr.success('Domain Updated', 'Success');
						this.refreshDataTable();
						$('#editData').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				delete: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.deleteDomain')) !!},{domain_id: this.current.id}).then(function(r){
						this.$set('process',false);
						toastr.success('Domain Deleted', 'Success');
						this.refreshDataTable();
						$('#deleteData').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				flush: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.flushDomain')) !!},{domain: this.current.domain,domain_id: this.current.id}).then(function(r){
						this.$set('process',false);
						toastr.success('Domain Flushed', 'Success');
						$('#flushData').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
			}
		});
		@include('admin.partials.js.CheckAlive')

		$('#datatables tbody').on('click', 'td button.edit-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			vue.$set('current',rowStats.data());
			$("#editData").modal();
		});

		$('#datatables tbody').on('click', 'td button.flush-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			vue.$set('current',rowStats.data());
			$("#flushData").modal();
		});

		$('#datatables tbody').on('click', 'td button.delete-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			vue.$set('current',rowStats.data());
			$("#deleteData").modal();
		});

		$('#datatables tbody').on('click', 'td button.open-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			var domain = rowStats.data();
			var asgh = 'http://';
			if(domain.ssl) asgh = 'https://';
			window.open(asgh+domain.domain, '_blank');
		});

	});
</script>
@endsection
