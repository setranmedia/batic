@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Categories</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<div class="pull-right">
							<button class="btn btn-success" data-toggle="modal" data-target="#grabCategories">Re-Grab All Categories</button>
							<button class="btn btn-info" data-toggle="modal" data-target="#addNew">Add New</button>
						</div>
						<form class="form-inline"s return="false">
							<div class="form-group">
								<select id="parent-id" class="form-control" v-model="parentId" v-on:change="refreshDataTable">
									<option :value="null">No Parent</option>
									<option v-for="cat in categories" :value="cat.id">@{{cat.name}}</option>
								</select>
							</div>
						</form>
					</div>
					<div class="box-body">
						<table id="datatables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#ID</th>
									<th>Name</th>
									<th>CID</th>
									<th>Query</th>
									<th>Products</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="overlay" v-if="process"><i class="fa fa-spinner fa-spin"></i></div>
				</div>
			</div>
		</div>
	</section>
	<div id="addNew" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="create">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">New Category</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="new-name">Name</label>
							<div class="col-md-9">
								<input type="text" id="new-name" class="form-control" v-model="default.name" required trim>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-parent_id">Parent</label>
							<div class="col-md-9">
								<select id="new-parent_id" class="form-control" v-model="default.parent_id">
									<option :value="">No Parent</option>
									<option v-for="cat in categories" :value="cat.id">@{{cat.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-cid">Alibaba CID</label>
							<div class="col-md-9">
								<input type="text" id="new-cid" class="form-control" v-model="default.cid">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-query">Alibaba Query</label>
							<div class="col-md-9">
								<input type="text" id="new-query" class="form-control" v-model="default.query">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="new-description">Description</label>
							<div class="col-md-9">
								<textarea id="new-description" class="form-control" v-model="default.description" placeholder="Some category description here..."></textarea>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Creating...</span>
							<span v-else>Create</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="editData" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="update">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Edit Category</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="current-name">Name</label>
							<div class="col-md-9">
								<input type="text" id="current-name" class="form-control" v-model="current.name" required>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-parent_id">Parent</label>
							<div class="col-md-9">
								<select id="current-parent_id" class="form-control" v-model="current.parent_id">
									<option :value="">No Parent</option>
									<option v-for="cat in categories" :value="cat.id" v-if="cat.id != current.id">@{{cat.name}}</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-cid">Alibaba CID</label>
							<div class="col-md-9">
								<input type="text" id="current-cid" class="form-control" v-model="current.cid">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-query">Alibaba Query</label>
							<div class="col-md-9">
								<input type="text" id="current-query" class="form-control" v-model="current.query">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="current-description">Description</label>
							<div class="col-md-9">
								<textarea id="current-description" class="form-control" v-model="current.description" placeholder="Some category description here..."></textarea>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Updating...</span>
							<span v-else>Update</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="deleteData" class="modal modal-danger fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="delete">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
						<h4 class="modal-title">Delete Category</h4>
					</div>

					<div class="modal-body">
						<p>Are you sure, want to delete this category <strong>@{{current.name}}</strong>?</p>
						<p v-if="current.has_childs" class="bg-yellow"><strong>Note:</strong> This category has some childs. Please select action for each child.</p>
						<div class="form-group" v-if="current.has_childs">
							<label class="control-label col-md-3" for="delete-child_action">Action</label>
							<div class="col-md-9">
								<select id="delete-child_action" class="form-control" v-model="deleteData.child_action">
									<option value="delete">Also Delete</option>
									<option value="parent">New Parent</option>
								</select>
							</div>
						</div>
						<div class="form-group" v-if="deleteData.child_action == 'parent'">
							<label class="control-label col-md-3" for="new-parent_id">New Parent</label>
							<div class="col-md-9">
								<select id="new-parent_id" class="form-control" v-model="deleteData.new_parent_id">
									<option :value="null">No Parent</option>
									<option v-for="cat in categories" :value="cat.id" v-if="cat.id != current.id">@{{cat.name}}</option>
								</select>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="process">
							<span v-if="process"><i class="fa fa-refresh fa-spin"></i> Deleting...</span>
							<span v-else>Delete</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div id="showProducts" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" :disabled="process">&times;</button>
					<h4 class="modal-title">@{{current.name}}</h4>
				</div>

				<div class="modal-body">
					<div class="box box-info box-solid">
						<div class="box-body">
							<table id="productstables" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th width="80%">Title</th>
										<th width="20%" class="text-center">Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="text-center">
						<button class="btn btn-success" type="button" v-if="process" disabled><i class="fa fa-refresh fa-spin"></i> Grabbing Products...</button>
						<button class="btn btn-success" type="button" v-on:click="grabProductsCategory(current.id)" v-else>Re-Grab Products</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="grabCategories" class="modal modal-primary fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal form-bordered" v-on:submit="grabbing">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" :disabled="grabber.process">&times;</button>
						<h4 class="modal-title">Grab @{{currentCount}} Categories</h4>
					</div>

					<div class="modal-body">
						<p>Langsung hajar bae.....</p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" :disabled="grabber.process">Close</button>
						<button type="submit" class="btn btn-outline" :disabled="grabber.process">
							<span v-if="grabber.process"><i class="fa fa-refresh fa-spin"></i> Grabbing...</span>
							<span v-else>Grab</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
@endsection
@section('vue')
<script type="text/javascript">
	$(document).ready(function($){
		Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
		var vue = new Vue({
			el: '#batic-content',
			data: {
				dataTable: {},
				productsTable: false,
				current: {},
				default: {},
				deleteData: {
					child_action: 'delete',
					new_parent_id: null
				},
				categories: {},
				currentCount: 0,
				parentId: null,
				process: false,
				grabber: {
					current: 0,
					process: false,
					categories:[]
				},
				csrfToken: $('meta[name="csrf-token"]').attr('content')
			},
			ready: function(){
				this.getDefault();
				this.getDataTable();
				this.getCategories();
				this.productsTable = $('#productstables').DataTable({
					data: [],
					columns: [
						{ data: 'title', name: 'title' },
						{
							"className": 'text-center',
							"orderable": false,
							"searchable": false,
							"data": null,
							"defaultContent": '<button type="button" class="btn btn-xs btn-primary view-product" title="View"><i class="fa fa-external-link"></i></button>'
						}
					]
				});
			},
			methods: {
				getDefault: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.config')) !!},{name:'batic.defaultCategory'}).then(function(r){
						this.$set('default',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getCategories: function(){
					this.$set('process',true);
					this.$http.post({!! json_encode(route('admin.ajax.get.categories')) !!},{parent_id:null}).then(function(r){
						this.$set('categories',r.data);
						this.$set('process',false);
					},function(r){
						this.$set('process',false);
					});
				},
				getDataTable: function(){
					var vueThis = this;
					this.dataTable = $('#datatables').DataTable({
						processing: true,
						serverSide: true,
						ajax: {
							url: {!! json_encode(route('admin.ajax.datatable.categories')) !!},
							type: 'POST',
							data: function (d) {
								d.parent_id = vueThis.parentId;
							},
							headers: { 'X-CSRF-TOKEN': vueThis.csrfToken }
						},
						columns: [
							{ data: 'id', name: 'id' },
							{ data: 'name', name: 'name' },
							{ data: 'cid', name: 'cid' },
							{ data: 'query', name: 'query' },
							{
								className: 'text-center',
								orderable: false,
								searchable: false,
								data: 'products',
								render: function(data, type, full, meta){
									return '<a href="#" class="show-products" title="Show Products">'+data.length+'</a>';
								}
							},
							{
								"className": 'text-center',
								"orderable": false,
								"searchable": false,
								"data": null,
								"defaultContent": '<button type="button" class="btn btn-xs btn-success edit-data" title="Edit"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-xs btn-danger delete-data" title="Delete"><i class="fa fa-trash-o"></i></button> <button type="button" class="btn btn-xs btn-primary open-data" title="View"><i class="fa fa-external-link"></i></button>'
							}
						],
						fnDrawCallback: function(settings, json){ 
							var info = this.api().page.info();
							vueThis.$set('currentCount',info.recordsTotal);
						}
					});
				},
				refreshDataTable: function(){
					this.dataTable.ajax.reload();
				},
				create: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.createCategory')) !!},{category: this.default}).then(function(r){
						this.$set('process',false);
						toastr.success('New Category Created', 'Success');
						this.refreshDataTable();
						this.getCategories();
						$('#addNew').modal('toggle');
						this.getDefault();
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				update: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.updateCategory')) !!},{category_id: this.current.id,category: this.current}).then(function(r){
						this.$set('process',false);
						toastr.success('Category Updated', 'Success');
						this.refreshDataTable();
						this.getCategories();
						$('#editData').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				delete: function(e){
					e.preventDefault();
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.deleteCategory')) !!},{category_id: this.current.id,params: this.deleteData}).then(function(r){
						this.$set('process',false);
						toastr.success('Category Deleted', 'Success');
						this.refreshDataTable();
						this.getCategories();
						$('#deleteData').modal('toggle');
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				grabbing: function(e){
					e.preventDefault();
					this.grabber.process = true;
					this.$http.post({!! json_encode(route('admin.ajax.get.categories')) !!},{parent_id:this.parentId}).then(function(r){
						this.grabber.categories = r.data;
						this.grabber.current = 0;
						this.grabCategory();
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.grabber.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
				grabCategory: function(){
					var grabber = this.grabber;
					var category = grabber.categories[(grabber.current)];
					if(typeof(category) == 'undefined'){
						this.grabber.process = false;
						this.refreshDataTable();
						$("#grabCategories").modal('hide');
					}else{
						this.$http.post({!! json_encode(route('admin.ajax.post.grabProductsCategory')) !!},{category_id: category.id}).then(function(r){
							toastr.success('Products grabbed', category.name);
							this.grabber.current += 1;
							this.grabCategory();
						},function(r){
							var message = 'Unknown response.';
							if(r.data.error) message = r.data.error;
							toastr.error(message, category.name);
							this.grabber.current += 1;
							this.grabCategory();
						});
					}
				},
				grabProductsCategory: function(category_id){
					this.$set('process',1);
					this.$http.post({!! json_encode(route('admin.ajax.post.grabProductsCategory')) !!},{category_id: category_id}).then(function(r){
						this.$set('process',false);
						toastr.success('Products List Updated', 'Success');
						this.productsTable.clear();
						this.productsTable.rows.add(r.data.products);
						this.productsTable.draw();
						this.refreshDataTable();
					},function(r){
						var message = 'Unknown response.';
						if(r.data.error) message = r.data.error;
						this.$set('process',false);
						toastr.error(message, 'Error');
					});
				},
			}
		});
		@include('admin.partials.js.CheckAlive')

		$('#datatables tbody').on('click', 'td a.show-products', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			var current = rowStats.data();
			vue.current = current;
			vue.productsTable.clear();
			vue.productsTable.rows.add(current.products);
			vue.productsTable.draw();
			$("#showProducts").modal();
		});

		$('#datatables tbody').on('click', 'td button.edit-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			var current = rowStats.data();
			vue.$set('current',current);
			$("#editData").modal();
		});

		$('#datatables tbody').on('click', 'td button.delete-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			vue.$set('current',rowStats.data());
			$("#deleteData").modal();
		});

		$('#datatables tbody').on('click', 'td button.open-data', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.dataTable.row( trStats );
			var category = rowStats.data();
			window.open(category.url, '_blank');
		});

		$('#productstables tbody').on('click', 'td button.view-product', function () {
			var trStats = $(this).closest('tr');
			var rowStats = vue.productsTable.row( trStats );
			var product = rowStats.data();
			window.open(product.url, '_blank');
		});
	});
</script>
@endsection