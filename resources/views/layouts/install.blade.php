<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BaTic 2 Installation</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ url('assets/dist/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/dist/css/skins/_all-skins.min.css') }}">
</head>
<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">
		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container">
					<div class="navbar-header"></div>
					<div class="navbar-custom-menu"><ul class="nav navbar-nav"></ul></div>
				</div>
			</nav>
		</header>
		<div class="content-wrapper">
			<div class="container">
				<section class="content">
					@yield('content')
				</section>
			</div>
		</div>
	</div>
	<footer class="main-footer">
		<div class="pull-right hidden-xs">BaTic 2</div>
		<strong>Copyright &copy; 2017 <a href="#">INSFIRES.COM</a>.</strong> All rights reserved.
	</footer>
	<script src="{{ url('assets/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
	<script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>

	<script src="{{ url('assets/plugins/vue/vue.min.js') }}"></script>
	<script src="{{ url('assets/plugins/vue/vue-resource.min.js') }}"></script>

	<script src="{{ url('assets/plugins/jQuery/jquery.toaster.js') }}"></script>

	<script src="{{ url('assets/dist/js/app.min.js') }}"></script>
	
	@yield('vue')
</body>
</html>