<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BaTic</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ url('assets/dist/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/dist/css/skins/_all-skins.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/plugins/datatables/dataTables.bootstrap.css') }}">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini" id="batic-content">
	<div class="wrapper">
		<header class="main-header">
			<a href="{!! route('admin.dashboard') !!}" class="logo"><span class="logo-mini"><b>B</b>T</span><span class="logo-lg"><b>Ba</b>Tic</span></a>

			<nav class="navbar navbar-static-top" role="navigation">
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span></a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li>
							<a href="{!! route('admin.logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

							<form id="logout-form" action="{!! route('admin.logout') !!}" method="POST" style="display: none;"><input type="hidden" name="_token" v-model="csrfToken"></form>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li><a href="#"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
					<li><a href="{!! route('admin.domains') !!}"><i class="fa fa-clone"></i> <span>Domains</span></a></li>
					<li><a href="{!! route('admin.categories') !!}"><i class="fa fa-tags"></i> <span>Categories</span></a></li>
					<li><a href="{!! route('admin.pages') !!}"><i class="fa fa-file-text-o"></i> <span>Manual Pages</span></a></li>
					<li><a href="{!! route('admin.seos') !!}"><i class="fa fa-sliders"></i> <span>SEO Rule</span></a></li>
					<li><a href="{!! route('admin.templates') !!}"><i class="fa fa-newspaper-o"></i> <span>Templates</span></a></li>
					<li><a href="{!! route('admin.keywords') !!}"><i class="fa fa-arrow-down"></i> <span>Keywords</span></a></li>
					<li><a href="{!! route('admin.queries') !!}"><i class="fa fa-search"></i> <span>Queries</span></a></li>
					<li class="treeview">
						<a href="#"><i class="fa fa-wrench"></i> <span>Settings</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
						<ul class="treeview-menu">
							<li><a href="{!! route('admin.settings.profile') !!}"><i class="fa fa-circle-o"></i> Profile</a></li>
							<li><a href="{!! route('admin.settings.permalinks') !!}"><i class="fa fa-circle-o"></i> Permalinks</a></li>
							<li><a href="{!! route('admin.settings.sitemaps') !!}"><i class="fa fa-circle-o"></i> Sitemaps</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Utilities</a></li>
						</ul>
					</li>
				</ul>
			</section>
		</aside>

		@yield('content')
		@include('admin.partials.dialog.login')
		<footer class="main-footer">
			<div class="pull-right hidden-xs">BaTic Version 2.0</div>
			<strong>Copyright &copy; 2017 <a href="http://btracker.co">BTracker</a>.</strong> All rights reserved.
		</footer>
	</div>
	<script src="{{ url('assets/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
	<script src="{{ url('assets/bootstrap/js/bootstrap.min.js') }}"></script>

	<script src="{{ url('assets/plugins/vue/vue.min.js') }}"></script>
	<script src="{{ url('assets/plugins/vue/vue-resource.min.js') }}"></script>
	
	<script src="{{ url('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ url('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ url('assets/plugins/datatables/handlebars.js') }}"></script>

	<script src="{{ url('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

	<script src="{{ url('assets/dist/js/app.min.js') }}"></script>
	<script type="text/javascript">
		toastr.options = {"positionClass": "toast-bottom-right","timeOut": "10000"}
	</script>
	@yield('vue')
</body>
</html>