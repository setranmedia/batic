<!DOCTYPE html>
<html>
    <head>
        <title>{!! $status_text !!}</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 75px;
                color: red;
            }
            .sub-title {
                font-size: 40px;
                color: #000;
                font-weight: bold;
                margin-bottom: 20px;
            }
            .message {
                font-size: 20px;
                font-style: italic;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">{!! $status_code !!}</div>
                <div class="sub-title">{!! $status_text !!}</div>
                <div class="message">Looks Like Something Wrong!!!. <a href="{!! url('/') !!}">Back to Home</a></div>
            </div>
        </div>
    </body>
</html>
