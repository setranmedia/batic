<?php return [
	[
		'name' => 'Default Home',
		'content' => 'Find quality Manufacturers, Suppliers, Exporters, Importers, Buyers, Wholesalers, Products and Trade Leads from china.',
		'type' => 'home',
	],
	[
		'name' => 'Default Category',
		'content' => '{!! $category->name !!}, Find Quality {!! $category->name !!} Products, {!! $category->name !!} Manufacturers, {!! $category->name !!} Suppliers and Exporters at {!! strtoupper($site->domain) !!}.',
		'type' => 'category',
	],
	[
		'name' => 'Default Keyword',
		'content' => '{!! ucwords($keyword->query) !!}, Find Quality {!! ucwords($keyword->query) !!} Products, {!! ucwords($keyword->query) !!} Manufacturers,{!! ucwords($keyword->query) !!} Suppliers and Exporters at {!! $site->site_name !!}',
		'type' => 'keyword',
	],
	[
		'name' => 'Default Search',
		'content' => '{!! ucwords($search->query) !!}, Find Quality {!! ucwords($search->query) !!} Products, {!! ucwords($search->query) !!} Manufacturers,{!! ucwords($search->query) !!} Suppliers and Exporters at {!! $site->site_name !!}',
		'type' => 'search',
	],
];