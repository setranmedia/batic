<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Watson\Rememberable\Rememberable;
use Storage;

class Category extends Model
{
	use Sluggable;
    use Rememberable;
	protected $guarded = ['updated_at','created_at'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    protected $casts = [
        'products' => 'array',
    ];

    public function childs()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = html_entity_decode(trim($value),ENT_QUOTES);
    }

    public function scopeNoParent($query)
    {
        return $this->where('parent_id', '');
    }

    public function hasParent()
    {
        return $this->parent_id > 0;
    }

    public function hasChild()
    {
        return $this->childs()->count() > 0;
    }

    public function domain()
    {
        return $this->belongsToMany('App\Domain');
    }

    public function getUrlAttribute(){
        return route('category',[$this->slug]);
    }

		public function getProductsAttribute(){
        $filename = 'categories/'.$this->id.'.txt';
        if(Storage::exists($filename)){
            $serialized = Storage::get($filename);
            if($this->isJson($serialized)){
                return json_decode($serialized,true);
            }
        }
        return [];
    }

    public function setProductsAttribute($value){
        $filename = 'categories/'.$this->attributes['id'].'.txt';
        if(is_array($value) && !empty($value)){
            Storage::put($filename,json_encode($value));
        }
        unset($this->attributes['products']);
    }

		protected function isJson($string) {
			json_decode($string);
			return (json_last_error() == JSON_ERROR_NONE);
		}
}
