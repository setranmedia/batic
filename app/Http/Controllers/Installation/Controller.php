<?php

namespace App\Http\Controllers\Installation;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\User;
use App\Http\Controllers\Controller AS BaseController;
use App\BTracker\Api AS BTrackerApi;
class Controller extends BaseController
{
	public function __construct()
    {
        $this->middleware('installation');
    }

	public function index(Request $request){
		$requirements = [];
		if(!version_compare(PHP_VERSION,"5.6.4",">="))
			$requirements[] = 'PHP version 5.6.4 or greater required, Please update your PHP version';
		if(!extension_loaded('openssl'))
			$requirements[]='OpenSSL PHP Extension is required, Please install this PHP extension';
		if(!extension_loaded('pdo_mysql'))
			$requirements[]='PDO MySQL PHP Extension is required, Please install this PHP extension';
		if(!extension_loaded('mbstring'))
			$requirements[]='Mbstring PHP Extension is required, Please install this PHP extension';
		if(!extension_loaded('tokenizer'))
			$requirements[]='Tokenizer PHP Extension is required, Please install this PHP extension';
		if(!extension_loaded('xml'))
			$requirements[]='XML PHP Extension is required, Please install this PHP extension';
		if(!extension_loaded('intl'))
			$requirements[]='Intl PHP Extension is required, Please install this PHP extension';

		if(!empty($requirements)){
			return view('installation.index',['status' => false,'requirements' => $requirements]);
		}
		
		$btracker = config('btracker');
		$status = [
			'btracker' => isset($btracker['token']) ? true : false,
		];
		$database = [
			'connection' => env('DB_CONNECTION'),
			'host' => env('DB_HOST'),
			'port' => env('DB_PORT'),
			'name' => env('DB_DATABASE'),
			'username' => env('DB_USERNAME'),
			'password' => env('DB_PASSWORD'),
			'status' => false
		];
		try {
			\DB::connection()->getPdo();
		}catch (\Exception $e) {
			die($e->getMessage());
		}
		if(\Schema::hasTable('migrations')){
			$database['status'] = true;
		}
		return view('installation.index',['status' => $status,'database' => $database]);
	}

	public function getToken(Request $request){
		if($request->has('email') && $request->has('password')){
			$email = $request->get('email');
			$password = $request->get('password');
				
			try {
				$domain = $this->getDomain();

				$BTrackerApi = new BTrackerApi;
				$BTrackerApi->refreshToken($email,$password,$domain);
				return response()->json(['status' => 'success']);
			}catch (\Exception $e) {
				return response()->json(['status' => 'failed','message'=>$e->getMessage()]);
			}
			return response()->json(['status' => $status]);
		}
	}

	public function migrateDatabase(Request $request){
		if($request->has('name') && $request->has('email') && $request->has('password')){
			try {
				\Artisan::call('migrate', array('--force' => true));
				User::create([
					'name' => $request->get('name'),
					'email' => $request->get('email'),
					'password' => bcrypt($request->get('password')),
				]);
				\Artisan::call('db:seed', array('--force' => true));
				return response()->json(['status' => 'success']);
			}catch (\Exception $e) {
				return response()->json(['status' => 'failed','message'=>$e->getMessage()]);
			}
		}
		return response()->json(['status' => 'failed','message'=> 'Incomplete form submited.']);
	}

	protected function getDomain(){
		$request=app('request');
		$host_domain = $request->header('host');
		if (substr($host_domain, 0, 4) == 'www.'){
			$host_domain = str_replace('www.','',$host_domain);
		}
		return $host_domain;
	}

	protected function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}
