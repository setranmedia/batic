<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
Use App\Query;
Use App\Keyword;
Use App\Page;
Use App\BTracker\Template AS BTemplate;
Use App\BTracker\Api AS BApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class SearchController extends Controller
{
	protected $cache_time; //minutes
	protected $layout = 'search';
	protected $site;
	protected $pages = false;
	protected $categories = false;
	protected $domain;
	protected $seo;
	protected $template;


	public function __construct(Request $request)
	{
		$this->middleware('publicWeb');
		$cache_time = $this->cache_time = env('CACHE_QUERY',0);
		$this->domain = $domain = str_replace('www.','',$request->header('host'));
		$this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
		if(!$this->site) die('Illegal domain pointed');
		if($this->site->ssl) \URL::forceSchema('https');

		$this->seo = Seo::where('id',$this->site->seo_search_id)->remember($this->cache_time)->cacheTags(['seo',$this->domain])->first();
		if(!$this->seo){
			$this->seo = new Seo(config('batic.defaultSeo.search'));
		}

		$this->template = Template::where('id',$this->site->template_search_id)->remember($this->cache_time)->cacheTags(['template',$this->domain])->first();
		if(!$this->template){
			$this->template = new Template(config('batic.defaultTemplate.search'));
		}
		if(is_array($this->site->categories)){
			$this->categories = Category::whereIn('id',$this->site->categories)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
		}
		if(is_array($this->site->pages)){
			$this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
		}
	}

	public function render(Request $request)
	{
		if(!$request->has('q')) return abort(404,"Page Not Found");
		$query = strtolower(trim($request->get('q')));
		$domain = $this->domain;
		$cache_time = $this->cache_time;

		try{
			$search = Query::where('query',$query)->where('type','search-form')->firstOrFail();
		}catch(\Exception $e){
			$search = new Query(['query' => $query,'type' => 'search-form']);
		}
		$search->total += 1;
		$search->save();

		$this->BApi = new BApi(true);

		$check = Keyword::where('query',$query)->first();
		if($check && count($check->results)){
			$search->results = $check->results;
			$search->type = $check->type;
		}else{
			$results = $this->getProducts($query);
			if(!$results) return abort(404,"empty");
			$search->results = $results['results'];
			$search->type = $results['type'];
		}
		$this->site->footer_script .= $this->BApi->offerJs($this->domain);
		Config::set('layout',$this->layout);
		Config::set('site',$this->site);
		Config::set('categories',$this->categories);
		Config::set('search',$search);
		Config::set('domain',$this->domain);
		Config::set('pages',$this->pages);

		$templateInit = new BTemplate([
			'site' => $this->site,
			'search' => $search,
		]);
		Config::set('description',$templateInit->render($this->template->content));
		\SEO::setTitle($templateInit->render($this->seo->meta_title));
		\SEO::setDescription($templateInit->render($this->seo->meta_description));
		SEOMeta::addKeyword($templateInit->render($this->seo->meta_keyword));
		SEOMeta::addMeta('robots', $this->seo->meta_robot);
		OpenGraph::addProperty('site_name', $this->site->site_name)
			->addProperty('type', 'object');
		Twitter::setType('summary');
		$layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
		if(file_exists($layoutFile)){
			$theme = Theme::uses($this->site->theme)->layout($this->layout);
			return $theme->render();
		}
		return view('public.'.$this->layout);
	}

	protected function getProducts($q){
		$key = $q;
		if($results = \Cache::tags('search')->get($key)){
			if(isset($results['results']) && is_array($results['results'])) return $results;
		}
		$results = $this->executeProducts($q);
		if(isset($results['results']) && is_array($results['results'])){
			$products = array_random($results['results'],$this->site->per_page);
			$results['results'] = is_array($products)?$products:[];
			if(!empty($results['results'])) \Cache::tags('search')->put($key,$results, $this->cache_time);
			return $results;
		}
		return false;
	}

	protected function executeProducts($q){
        try{
            $products = $this->BApi->getProducts(false,$q);
            return [
                'type' => 'products',
                'results' => $products
            ];
        }catch(\Exception $e){
            return $this->executeGoogleImages($q);
        }

    }

    protected function executeGoogleImages($q){
        try{
            $google_images = $this->BApi->getGoogleImages($q);
            return [
                'type' => 'google_images',
                'results' => $google_images
            ];
        }catch(\Exception $e){
            return false;
        }

    }
}
