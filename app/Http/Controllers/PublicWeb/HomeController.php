<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
Use App\Page;

Use App\BTracker\Template AS BTemplate;
Use App\BTracker\Api AS BApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class HomeController extends Controller
{
  protected $cache_time; //minutes
  protected $layout = 'home';
  protected $site;
  protected $pages = false;
	protected $categories = false;
  protected $domain;
  protected $seo;
	protected $template;

  public function __construct(Request $request)
  {
    $this->middleware('publicWeb');
    $cache_time = $this->cache_time = env('CACHE_QUERY',0);
    $this->domain = $domain = str_replace('www.','',$request->header('host'));
    $this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
    if(!$this->site) die('Illegal domain pointed');
    if($this->site->ssl) \URL::forceSchema('https');
    $this->seo = Seo::where('id',$this->site->seo_home_id)->remember($this->cache_time)->cacheTags(['seo',$this->domain])->first();
    if(!$this->seo){
      $this->seo = new Seo(config('batic.defaultSeo.home'));
    }
    $this->template = Template::where('id',$this->site->template_home_id)->remember($this->cache_time)->cacheTags(['template',$this->domain])->first();
    if(!$this->template){
      $this->template = new Template(config('batic.defaultTemplate.home'));
    }
    if(is_array($this->site->categories)){
      $this->categories = Category::whereIn('id',$this->site->categories)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
    }
    if(is_array($this->site->pages)){
      $this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
    }
  }

  public function render()
  {
    $this->products = $this->getProducts();

    $BApi = new BApi;
    $this->site->footer_script .= $BApi->offerJs($this->domain);
    $templateInit = new BTemplate([
      'site' => $this->site
    ]);
    if($this->categories){
      foreach ($this->categories as $key => $cat) {
        $this->categories[$key]->products = [];
      }
    }
    Config::set('layout',$this->layout);
    Config::set('site',$this->site);
    Config::set('categories',$this->categories);
    Config::set('pages',$this->pages);
    Config::set('domain',$this->domain);
    Config::set('products',$this->products);
    Config::set('description',$templateInit->render($this->template->content));
    \SEO::setTitle($templateInit->render($this->seo->meta_title));
    \SEO::setDescription($templateInit->render($this->seo->meta_description));
    SEOMeta::addKeyword($templateInit->render($this->seo->meta_keyword));
    SEOMeta::addMeta('robots', $this->seo->meta_robot);
    SEOMeta::setCanonical(route('home'));
    OpenGraph::setUrl(route('home'))->addProperty('site_name', $this->site->site_name)->addProperty('type', 'website');
    Twitter::setType('summary');
    if($this->products){
      $product = array_first($this->products, function ($value, $key) {
        return $value['image'];
      });
      OpenGraph::addProperty('image',$product['image']);
      Twitter::addImage($product['image']);
    }
    $layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
    if(file_exists($layoutFile)){
      return Theme::uses($this->site->theme)->layout($this->layout)->render();
    }
    return view('public.'.$this->layout);
  }

  protected function getProducts(){
    $key = 'index';
    $filename = 'products-cache/'.$this->domain.'/index.txt';
    if(\Storage::exists($filename)){
        $serialized = \Storage::get($filename);
        if(isJson($serialized)){
            return json_decode($serialized,true);
        }
    }
    if($this->categories){
      $products = [];
      $categories = $this->categories;
      foreach($categories as $category){
        $productsCat = getProductsByCategory($category,$this->site);
        if(is_array($productsCat)) $products = array_merge($products,$productsCat);
      }
      $products = array_random($products,$this->site->per_page);
      if(!empty($products)) \Storage::put($filename,json_encode($products));
      return $products;
    }
  }
}
