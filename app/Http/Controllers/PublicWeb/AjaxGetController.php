<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AjaxGetController extends Controller
{
    public function checkAlive(Request $request){
    	$loggedIn = false;
    	if(Auth::check()){
    		$loggedIn = true;
    	}
    	return response()->json(['logged_in' => $loggedIn,'csrf_token' => csrf_token()]);
    }
}
