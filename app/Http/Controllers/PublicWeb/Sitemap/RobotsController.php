<?php

namespace App\Http\Controllers\PublicWeb\Sitemap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Keyword;
use App\Category;
Use App\Page;
use App\Domain;

class RobotsController extends Controller
{
    protected $query_cache;
    protected $sitemap_cache;
    protected $site;
    protected $domain;
    protected $per_page;


    public function __construct(Request $request)
    {
    	$this->middleware('publicWeb');
    	$cache_time = $this->query_cache = env('CACHE_QUERY',0);
      $this->sitemap_cache = env('SITEMAP_CACHE',10080);
      $this->per_page = config('batic.sitemaps.url_per_file',10000);
    	$this->domain = $domain = str_replace('www.','',$request->header('host'));
    	$this->site = Domain::where('domain',$this->domain)->remember($this->query_cache)->first();
    	if(!$this->site) die('Illegal domain pointed');
      if($this->site->ssl) \URL::forceSchema('https');
    }

    public function render(){
    	$key = $this->domain.'.robots.txt';
    	if(!$txt = cache($key)){
        $txt = $this->buildRobots();
        if(!empty($txt)) cache([$key => $txt], $this->sitemap_cache);
      }
    	return response()->make($txt)->header('content-type','text/plain');
    }

    protected function buildRobots(){
    	$xls = url('sitemap.xsl');
    	$now = date('c', time());
    	$txt = 'User-agent: *'."\n";
      $txt .= 'Disallow: /admin/'."\n";
      $txt .= 'Disallow: /ajax/'."\n";
      $txt .= "\n";
      $txt .= 'Sitemap: '.route('sitemap.index')."\n";
      $txt .= 'Sitemap: '.route('sitemap.main')."\n";
      $keywords = Keyword::remember($this->query_cache)->prefix($this->domain)->count();
      $total = ceil($keywords/$this->per_page);
      if($total){
        for($i=1;$i<=$total;$i++){
          $txt .= 'Sitemap: '.route('sitemap.keyword',[$i])."\n";
        }
      }
      return $txt;
    }
}
