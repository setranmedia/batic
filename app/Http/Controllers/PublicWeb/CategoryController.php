<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Domain;
Use App\Seo;
Use App\Category;
Use App\Template;
use App\Page;

Use App\BTracker\Template AS BTemplate;
Use App\BTracker\Api AS BApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class CategoryController extends Controller
{
	protected $cache_time; //minutes
	protected $layout = 'category';
	protected $site;
	protected $pages = false;
	protected $categories = false;
	protected $domain;
	protected $seo;
	protected $template;


	public function __construct(Request $request)
	{
		$this->middleware('publicWeb');
		$cache_time = $this->cache_time = env('CACHE_QUERY',0);
		$this->domain = $domain = str_replace('www.','',$request->header('host'));
		$this->site = Domain::where('domain',$this->domain)->remember($this->cache_time)->cacheTags(['domain',$this->domain])->first();
		if(!$this->site) die('Illegal domain pointed');
		if($this->site->ssl) \URL::forceSchema('https');
		$this->seo = Seo::where('id',$this->site->seo_category_id)->remember($this->cache_time)->cacheTags(['seo',$this->domain])->first();

		if(!$this->seo){
			$this->seo = new Seo(config('batic.defaultSeo.category'));
		}
		$this->template = Template::where('id',$this->site->template_category_id)->remember($this->cache_time)->cacheTags(['template',$this->domain])->first();

		if(!$this->template){
			$this->template = new Template(config('batic.defaultTemplate.category'));
		}
    if(is_array($this->site->categories)){
      $this->categories = Category::whereIn('id',$this->site->categories)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['categories',$this->domain])->get();
    }
    if(is_array($this->site->pages)){
      $this->pages = Page::whereIn('id',$this->site->pages)->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages',$this->domain])->get();
    }
	}

	public function render($slug)
	{

		try{
			$domain = $this->domain;
			$cache_time = $this->cache_time;
			$this->category = Category::where('slug',$slug)
				->with([
					'childs' => function ($q) use($cache_time) { $q->remember($cache_time); },
					'parent' => function ($q) use($cache_time) { $q->remember($cache_time); },
				])
				->remember($this->cache_time)->cacheTags('categories')->firstOrFail();
		}catch(\Exception $e){
			return abort(404);
		}
		$this->products = $this->getProducts();
		if($this->categories){
			foreach ($this->categories as $key => $cat) {
				$this->categories[$key]->products = [];
			}
		}
		$BApi = new BApi;
		$this->site->footer_script .= $BApi->offerJs($this->domain);

		Config::set('layout',$this->layout);
		Config::set('site',$this->site);
		Config::set('categories',$this->categories);
		Config::set('category',$this->category);
		Config::set('domain',$this->domain);
		Config::set('products',$this->products);
		Config::set('pages',$this->pages);

		$templateInit = new BTemplate([
			'site' => $this->site,
			'category' => $this->category,
		]);
		Config::set('description',$templateInit->render($this->template->content));
		\SEO::setTitle($templateInit->render($this->seo->meta_title));
		\SEO::setDescription($templateInit->render($this->seo->meta_description));
		SEOMeta::addKeyword($templateInit->render($this->seo->meta_keyword));
		SEOMeta::addMeta('robots', $this->seo->meta_robot);
		SEOMeta::setCanonical($this->category->url);
		OpenGraph::setUrl($this->category->url)
			->addProperty('site_name', $this->site->site_name)
			->addProperty('type', 'object');
		Twitter::setType('summary');
		if($this->products){
			$product = array_first($this->products, function ($value, $key) {
				return $value['image'];
			});
			OpenGraph::addProperty('image',$product['image']);
			Twitter::addImage($product['image']);
		}
		$layoutFile = public_path('themes'.DIRECTORY_SEPARATOR.$this->site->theme.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.$this->layout.'.php');
		if(file_exists($layoutFile)){
			$theme = Theme::uses($this->site->theme)->layout($this->layout);
			return $theme->render();
		}
		return view('public.'.$this->layout);
	}

	protected function getProducts(){
		$categoryKey = $this->category->id;
		$products = [];
		$productsCat = getProductsByCategory($this->category,$this->site);
		if(is_array($productsCat) && !empty($productsCat)) return $productsCat;
		if(count($this->category->childs)>0){
			$productsCat = [];
			$childs = $this->category->childs;
			foreach($childs as $child){
				$productChild = getProductsByCategory($child,$this->site);
				if($productChild)
					$productsCat = array_merge($productsCat,$productChild);
			}
		}
		if(is_array($productsCat))
			$products = array_random($productsCat,$this->site->per_page);
		if(!empty($products)) \Storage::put('products-cache/'.$this->domain.'/'.$this->category->id.'.txt',json_encode($products));

		return $products;
	}
}
