<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller AS BaseController;

class Controller extends BaseController
{
    public function __construct()
    {
        $this->middleware('public');
    }

    public function home(Request $request){
    	return response()->json(['status' => 'success']);
    }
}
