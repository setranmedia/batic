<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function dashboard(Request $request){
		return view('admin.dashboard');
	}

	public function domains(Request $request){
		return view('admin.domains');
	}

	public function categories(Request $request){
		return view('admin.categories');
	}

	public function pages(Request $request){
		return view('admin.pages');
	}

	public function seos(Request $request){
		return view('admin.seos');
	}

	public function templates(Request $request){
		return view('admin.templates');
	}

	public function keywords(Request $request){
		return view('admin.keywords');
	}

	public function queries(Request $request){
		return view('admin.queries');
	}

	public function settingProfile(Request $request){
		return view('admin.settings.profile');
	}

	public function settingPermalinks(Request $request){
		return view('admin.settings.permalinks');
	}

	public function settingSitemaps(Request $request){
		return view('admin.settings.sitemaps');
	}
}
