<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use App\Category;
use App\Domain;
use App\Keyword;
use App\Page;
use App\Seo;
use App\Template;
use App\User;
use App\Queue;
use App\Query;

class DataTableController extends Controller
{
	public function domains(Request $request){
		$dataTables = Domain::all();
		return Datatables::of($dataTables)->make(true);
	}

	public function categories(Request $request){
		$parent_id = $request->has('parent_id')?$request->get('parent_id'):NULL;
		$dataTables = Category::where('parent_id',$parent_id);
		
		return Datatables::of($dataTables)
			->addColumn('url',function($category){
				return $category->url;
			})->editColumn('products',function($category){
				$products = (array) $category->products;
				return array_values($products);
			})->addColumn('has_childs',function($category){
				return !$category->parent_id;
			})->make(true);
	}

	public function pages(Request $request){
		$dataTables = Page::all();
		return Datatables::of($dataTables)
			->addColumn('url',function($page){
				return $page->url;
			})->make(true);
	}

	public function seos(Request $request){
		if($request->has('type')){
			return Datatables::of(Seo::where('type',$request->get('type')))->make(true);
		}
	}

	public function templates(Request $request){
		if($request->has('type')){
			return Datatables::of(Template::where('type',$request->get('type')))->make(true);
		}
	}

	public function keywords(Request $request){
		if($request->has('type')){
			return Datatables::of(Keyword::where('type',$request->get('type'))->select('id','query','type','slug'))
				->addColumn('url',function($keyword){
					return $keyword->url;
				})
				->removeColumn('results')
				->make(true);
		}
	}

	public function queries(Request $request){
		if($request->has('type')){
			return Datatables::of(Query::where('type',$request->get('type'))->select('id','query','type','total'))->make(true);
		}
	}

	public function queues(Request $request){
		return Datatables::of(Queue::all())->make(true);
	}
}
