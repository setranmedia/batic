<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Domain;
use App\Keyword;
use App\Page;
use App\Seo;
use App\Template;
use App\User;
use App\Queue;
use App\BTracker\Api AS BTrackerApi;

class PostController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function createDomain(Request $request){
		if($request->has('domain')){
			 try{
			 	$data = $request->get('domain');
			 	Domain::create($data);
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateDomain(Request $request){
		if($request->has('domain_id') && $request->has('domain')){
			try{
				$data = $request->get('domain');
				$domain = Domain::where('id',$request->get('domain_id'))->first();
				$domain->update($data);
				\Cache::tags('domain')->flush();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function deleteDomain(Request $request){
		if($request->has('domain_id')){
			try{
				Domain::where('id',$request->get('domain_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function flushDomain(Request $request){
		if($request->has('domain') && $request->has('domain_id')){
			try{
				\Cache::tags($request->get('domain'))->flush();
				\Artisan::call('batic:generate-cache',['--domain'=>$request->get('domain_id')]);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function createCategory(Request $request){
		if($request->has('category')){
			 try{
			 	$data = $request->get('category');
			 	Category::create([
					'name' => trim($data['name']),
					'parent_id' => isset($data['parent_id']) && trim($data['parent_id']) ? $data['parent_id'] : NULL,
					'cid' => trim($data['cid']),
					'query' => trim($data['query']),
					'description' => $data['description'],
				]);
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateCategory(Request $request){
		if($request->has('category_id') && $request->has('category')){
			try{
				$data = $request->get('category');
				$category = Category::where('id',$request->get('category_id'))->first();
				$category->update([
					'name' => trim($data['name']),
					'parent_id' => isset($data['parent_id']) && trim($data['parent_id']) ? $data['parent_id'] : NULL,
					'cid' => trim($data['cid']),
					'query' => trim($data['query']),
					'description' => $data['description'],
				]);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function deleteCategory(Request $request){
		if($request->has('category_id') && $request->has('params')){
			try{
				$category = Category::where('id',$request->get('category_id'))->first();
				$params = $request->get('params');
				if(isset($params['child_action']) && $params['child_action'] == 'delete'){
					Category::where('parent_id',$request->get('category_id'))->delete();
				}elseif(isset($params['child_action']) && $params['child_action'] == 'parent'){
					foreach ($category->childs as $child) {
						$child->parent_id = $params['new_parent_id'];
						$child->save();
					}
				}
				$category->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function createPage(Request $request){
		if($request->has('page') && $request->has('seo')){
			 try{
			 	$data = $request->get('page');
			 	if(!$data['seo_id']){
			 		$seo = Seo::create($request->get('seo'));
			 		$data['seo_id'] = $seo->id;
			 	}
			 	Page::create($data);
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updatePage(Request $request){
		if($request->has('page_id') && $request->has('page') && $request->has('seo')){
			try{
				$data = $request->get('page');
				$page = Page::where('id',$request->get('page_id'))->first();
				if(!$data['seo_id']){
			 		$seo = Seo::create($request->get('seo'));
			 		$data['seo_id'] = $seo->id;
			 	}
				$page->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function deletePage(Request $request){
		if($request->has('page_id')){
			try{
				Page::where('id',$request->get('page_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function createSeo(Request $request){
		if($request->has('seo')){
			try{
				Seo::create($request->get('seo'));
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateSeo(Request $request){
		if($request->has('seo_id') && $request->has('seo')){
			try{
				$data = $request->get('seo');
				$seo = Seo::where('id',$request->get('seo_id'))->first();
				$seo->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function deleteSeo(Request $request){
		if($request->has('seo_id')){
			try{
				Seo::where('id',$request->get('seo_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function createTemplate(Request $request){
		if($request->has('template')){
			try{
				Template::create($request->get('template'));
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateTemplate(Request $request){
		if($request->has('template_id') && $request->has('template')){
			try{
				$data = $request->get('template');
				Template::where('id',$request->get('template_id'))->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function deleteTemplate(Request $request){
		if($request->has('template_id')){
			try{
				Template::where('id',$request->get('template_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function createQueue(Request $request){
		if($request->has('keywords')){
			try{
				$keywords = explode("\n",$request->get('keywords'));
				$keywords = array_map('strtolower', $keywords);
				$keywords = array_unique($keywords);
				$queue = Queue::create(['total'=>count($keywords)]);
				$filename = 'queues'.DIRECTORY_SEPARATOR.$queue->id.'.txt';
				\Storage::put($filename,json_encode($keywords));
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function deleteQueue(Request $request){
		if($request->has('queue_id')){
			try{
				Queue::where('id',$request->get('queue_id'))->delete();
				$dir = 'queues'.DIRECTORY_SEPARATOR.$request->get('queue_id').'.txt';
				\Storage::delete($dir);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function deleteKeyword(Request $request){
		if($request->has('keyword_id')){
			try{
				Keyword::where('id',$request->get('keyword_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateProfile(Request $request){
		if($request->has('name') && $request->has('email')){
			 try{
				$user = $request->user();
				$user->name = $request->get('name');
				$user->email = $request->get('email');
				$user->save();
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updatePassword(Request $request){
		if($request->has('old') && $request->has('new')){
			 try{
				$user = $request->user();
				$credentials = ['email' => $user->email,'password' => $request->get('old')];
				if(\Auth::validate($credentials)){
					$user->password = bcrypt($request->get('new'));
					$user->save();
					return response()->json(['success' => true]);
				}else{
					return response()->json(['error' => 'Wrong current password.'],500);
				}
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateBatic(Request $request){
		if($request->has('name') && $request->has('values')){
			 try{
				$old = config('batic.'.$request->get('name'));
				if(!$old || !is_array($old))
					return response()->json(['error' => 'Unusual activity detected'],500);
				$main = array_merge($old,$request->get('values'));
				$save_data = var_export($main, 1);
				if(\File::put(config_path() . '/batic/'.$request->get('name').'.php', "<?php\n return $save_data ;")){
					if($request->get('name') == 'sitemaps') \Cache::tags('sitemap')->flush();
					return response()->json(['success' => true]);
				}
				return response()->json(['error' => 'Failed to save.'],500);
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function grabProductsCategory(Request $request){
		if($request->has('category_id')){
			try{
				$category = Category::where('id',$request->get('category_id'))->with('childs')->first();
				$BTrackerApi = new BTrackerApi;
				$results = $BTrackerApi->getProducts($category->cid,$category->query);
				$category->products = $results;
				$category->save();
				return response()->json(['products'=>array_values($results)]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}
}
