<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use App\Domain;

class PublicWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        spp_setinfo();
        $redirects = config('batic.redirect',['www' => [],'non_www'=>[]]);
        if(!empty($redirects['www']) || !empty($redirects['non_www'])){
            $host = $request->header('host');
            if(!empty($redirects['www']) && in_array($host,$redirects['www'])){
                $request->headers->set('host', 'www.'.$host);
                return redirect($request->path());
            }elseif(!empty($redirects['non_www']) && substr($host, 0, 4) == 'www.' && in_array(str_replace('www.','',$host),$redirects['non_www'])){
                $request->headers->set('host', str_replace('www.','',$host));
                    return redirect($request->path());
            }
        }
        return $next($request);
    }
}
