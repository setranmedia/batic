<?php

namespace App\Http\Middleware;

use Closure;

class Installation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!env('RUN_INSTALLATION',false)){
            return abort(403, 'Unauthorized action.');
        }
        return $next($request);
    }
}
