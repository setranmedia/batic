<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class Query extends Model
{
    use Rememberable;
    public $timestamps = false;
    protected $guarded = [];

}
