<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \jdavidbakr\LaravelCacheGarbageCollector\LaravelCacheGarbageCollector::class,
        Commands\QueueKeyword::class,
        Commands\QueueCategory::class,
        Commands\BaticInstall::class,
        Commands\BaticUpdate::class,
        Commands\CreateAdmin::class,
        Commands\CreateDomain::class,
        Commands\ImportSeo::class,
        Commands\ImportTemplate::class,
        Commands\ImportCategory::class,
        Commands\GenerateCache::class,
        Commands\GenerateProducts::class,
        Commands\ClearAndGenerateCache::class,
        Commands\LoopQueueKeyword::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('batic:queue-keywords')->cron(env('QUEUE_KEYWORD_CRON','0 * * * *'));
        $schedule->command('batic:queue-categories')->cron(env('QUEUE_CATEGORY_CRON','12 */8 * * *'));
        $schedule->command('batic:clear-and-generate-cache')->weekly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
