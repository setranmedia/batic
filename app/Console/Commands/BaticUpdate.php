<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BaticUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batic:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upgrade utility controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->btracker = config('btracker');
        $this->current_version = config('btracker.version','1.0.0');
        $this->update_version = config('app.version','1.0.0');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('*Run database migration...');
        $this->call('migrate',['--force' => true]);
        if(version_compare($this->current_version,'1.0.1','<')){
          $this->call('batic:install',['--refresh-token' => true]);
        }
        $this->info('*Update complete...');
    }
}
