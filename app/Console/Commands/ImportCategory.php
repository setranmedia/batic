<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Category;

class ImportCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batic:import-categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import SEO Rule from pre-defined configuration';

    protected $db_connection = false;
    protected $db_table_exist = false;
    protected $data_to_import = false;
    protected $data_path = 'import.categories';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkDbConnection();
        $this->checkMigration();
        $this->checkData();
        if($this->db_connection && $this->db_table_exist && $this->data_to_import){
            $this->comment('***************************************');
            $this->comment('*          Import Categories          *');
            $this->comment('***************************************');
            $categories = Category::all(['id','name', 'cid','query']);
            if($count = count($categories)){
                if ($this->confirm('*Kategori wis ana wa!!!. Ana: '.$count.' kategori. Arep ndeleng?')) {
                    $headers = ["#ID",'Name', 'CID','Query'];
                    $this->table($headers, $categories);
                }
                if(is_array($this->data_to_import)){
                    if ($this->confirm('*Ana '.count($this->data_to_import).' kategori sing bisa diimport. Arep diimport ra?')) {
                        $this->importDefault();
                    }
                }
            }else{
                if(is_array($this->data_to_import)){
                    if ($this->confirm('*Ana '.count($this->data_to_import).' kategori sing bisa diimport. Arep diimport ra?')) {
                        $this->importDefault();
                    }
                }
            }
        }
    }

    protected function importDefault(){
        $categories = $this->data_to_import;
        $success = $failed = 0;
        foreach($categories as $category){
            try{
                Category::create($category);
                $this->line('*Category: \''.$category['name'].'\'... <info>Sukses</info>');
                $success +=1;
            }catch(\Exception $e){
                $this->line('*Category: \''.$category['name'].'\'... <error>Gagal</error>');
                $failed +=1;
            }
        }
        $this->info('');
        $this->comment('***************************************');
        $this->line('<info>Sukses: '.$success.'</info>, <error>Gagal : '.$failed.'</error>');
        $this->comment('***************************************');
        $this->info('');
    }

    protected function checkDbConnection(){
        try {
            \DB::connection()->getPdo();
            $this->db_connection = true;
        }catch (\Exception $e) {
            $this->line('*<error>Gagal konek meng DB :</error> Jal cek konfigurasi DBne maning nang file <comment>\'.env\'</comment> wis bener apa urung.');
            $this->db_connection = false;
        }
    }

    protected function checkMigration(){
        if($this->db_connection){
            if(\Schema::hasTable('categories'))
                $this->db_table_exist = true;
            else
                $this->line('*<error>Tabel \'categories\' ora ana je</error>... migrasi DBne salah mbok... jal running kiye <comment>\'php artisan migrate\'</comment>.');
        }
    }

    protected function checkData(){
        try {
            $this->data_to_import = config($this->data_path);
        }catch (\Exception $e) {
            $this->data_to_import = false;
            $this->line('*<error>Format datane ora valid</error>... jal cek maning <comment>\'config.import.categories.php\'</comment>');
        }
    }
}
