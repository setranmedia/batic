<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\BTracker\Api AS BTrackerApi;
use App\Category;
use App\Domain;

class QueueCategory extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'batic:queue-categories';
	protected $products = [];
	protected $blockIds = [];
	protected $blockIdsKey = 'batic.blockIds';
	protected $BTrackerApi;

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update category products list';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		if($blockIds = cache($this->blockIdsKey)){
			if(is_array($blockIds)){
				$this->blockIds = $blockIds;
			}
		}
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->comment('Sit...Enteni...Njiot Kategori sing paling mambu...');
		if(is_array($this->blockIds) && !empty($this->blockIds))
			$category = Category::whereNotIn('id',$this->blockIds)->where('parent_id',NULL)->with('childs')->first();
		else
			$category = Category::where('parent_id',NULL)->with('childs')->first();

		$this->sites = Domain::select('domain','per_page')->get();

		if($category){
			$this->line('<comment>Mroses Kategori:</comment> <info>'.$category->name.'</info>...');
			$this->BTrackerApi = new BTrackerApi;
			$childs = $category->childs;
			foreach ($childs as $child) {
				$this->output->write('Mroses Anake: <comment>'.$child->name.'</comment>...');
				$results = $this->executeProducts($child->cid,$child->query);
				if($results && is_array($results)){
					$this->output->writeln('<info>OK</info>');
					$child->products = $results;
					$child->save();
				}else{
					$this->output->writeln('<error>Produk Kosong</error>');
				}
				$this->generateCacheChild($child);
				$this->blockIds[] = $category->id;
			}
			$this->generateCacheParent($category);
			cache([$this->blockIdsKey => $this->blockIds], env('PRODUCTS_CACHE',10080));
			$this->info('Finish...');
		}else{
			$this->error("Ora ana kategori mambu sing bisa di-nget.");
		}
	}

	protected function executeProducts($cid,$q){
		try{
			$products = $this->BTrackerApi->getProducts($cid,$q);
			return $products;
		}catch(\Exception $e){
			return false;
		}
	}

	protected function generateCacheChild($category){
		foreach ($this->sites as $site) {
			$cache = getProductsByCategory($category,$site,false);
			if($cache){
				$this->products = array_merge($this->products,$cache);
			}
		}
	}

	protected function generateCacheParent($category){
		if(is_array($this->products) && !empty(is_array($this->products))){
			$cache_time = env('PRODUCTS_CACHE',10080);
			foreach ($this->sites as $site) {
				$categoryKey = $site->domain.'.products.'.$category->id;
				$productsCat = array_random($this->products,$site->per_page);
				$productsCat = is_array($productsCat)?$productsCat:false;
        		if($productsCat){
        			cache([$categoryKey => $productsCat], $cache_time);
        			$this->info('Cache OK: '.$site->domain.'...');
        		}else{
					$this->error('Gagal nggenerate cache domain: '.$site->domain.'...');
				}
			}
		}else{
			$this->error('Gagal nggenerate cache domain: '.$site->domain.'...');
		}
	}
}
