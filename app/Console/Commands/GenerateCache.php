<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Domain;
use App\Category;

class GenerateCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batic:generate-cache {--domain=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate products or results cache for each domain';
    protected $domains = false;
    protected $categories = false;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkDomain();
        if($this->domains){
            $cache_time = env('PRODUCTS_CACHE',10080);
            foreach ($this->domains as $domain) {
                $this->comment('**************************************');
                $this->comment('*Domain: '.$domain->domain);
                $this->comment('**************************************');
                if(is_array($domain->categories)){
                    $products = [];
                    $categories = Category::whereIn('id',$domain->categories)->get();
                    if($categories){
                        foreach ($categories as $category) {
                            $this->output->write('Mroses kategori: '.$category->name.'... ');
                            $produck = $this->generateCache($domain,$category);
                            if($produck){
                                $products = array_merge($products,$produck);
                            }
                        }
                        if(is_array($products) && !empty(($products))){
                            $domKey = 'index';
                            $productsDom = array_random($products,$domain->per_page);
                            $productsDom = is_array($productsDom)?$productsDom:false;
                            if($productsDom){
                              \Storage::put('products-cache/'.$domain->domain.'/index.txt',json_encode($productsDom));
                            }
                        }
                    }else{
                        $this->error('Ora ndue kategori');
                    }
                }else{
                    $this->error('Ora ndue kategori');
                }
            }
        }
    }

    protected function checkDomain(){
        if(!$domains = $this->option('domain')){
            $this->line('Lebokna ID domain, Pisah karo koma. Nek arep mroses kabeh domain ya ketik <comment>\'all\'</comment>');
            $domains = $this->ask('ID Domain');
        }
        try {
            if($domains == 'all'){
                $domains = Domain::select('domain','categories','per_page')->get();
            }else{
                $domains = Domain::whereIn('id',explode(',',$domains))->select('domain','categories','per_page')->get();
            }

            if($domains)
                $this->domains = $domains;
            else
                $this->error('Error wa pas ngecek domainne... bener apa ora nggone nglebokna IDne');
        }catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    protected function generateCache($domain,$category){
        if($category->query != '0' && is_array($category->products) && !empty($category->products)){
            $cache = getProductsByCategory($category,$domain,false);
            if($cache){
                return $cache;
                $this->output->writeln('<info>OK</info>');
            }else{
                $this->output->writeln('<error>Ora nduwe produk</error>');
            }
        }elseif($category->hasChild()){
            $childs = $category->childs;
            $products = [];
            $cache_time = env('PRODUCTS_CACHE',10080);
            $categoryKey = $category->id;
            foreach($childs as $child){
                $cache = getProductsByCategory($child,$domain,false);
                if($cache){
                    $products = array_merge($products,$cache);
                }
            }
            if(is_array($products) && !empty(($products))){
                $productsCat = array_random($products,$domain->per_page);
                $productsCat = is_array($productsCat)?$productsCat:false;
                if($productsCat){
                    \Storage::put('products-cache/'.$domain->domain.'/'.$category->id.'.txt',json_encode($productsCat));
                    $this->output->writeln('<info>OK</info>');
                    return $productsCat;
                }else{
                    $this->output->writeln('<error>Ora ndue produk, anake juga ora ndue produk</error>');
                }
            }else{
                $this->output->writeln('<error>Ora ndue produk, anake juga ora ndue produk</error>');
            }
        }else{
            $this->output->writeln('<error>Ora ndue produk juga ora ndue anak</error>');
        }
        return false;
    }
}
