<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BTracker\Api AS BTrackerApi;

use App\User;
use App\Seo;
use App\Template;
use App\Category;
use App\Domain;

class BaticInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batic:install {--refresh-token=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BaTic Installation';

    protected $db_connection = false;
    protected $db_migrated = false;
    protected $status = false;
    protected $domain;
    protected $btracker;
    protected $user;
    protected $BTrackerApi;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->btracker = config('btracker');
        $this->BTrackerApi = new BTrackerApi;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->generateKey();
      $this->checkUser();
      $this->checkDomain();
      $this->migrateDB();
      $this->createUser();
      $this->importDefault();
    }

    protected function generateKey(){
      if(!isset($this->btracker['install']['key'])){
        $this->btracker['install']['key'] = true;
        $this->call('key:generate');
        \Config::set('btracker',$this->btracker);
        try{
          $this->BTrackerApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->btracker['install']['key']);
            $this->generateKey();
          }
        }
      }else{
        $this->comment('*BaTic KEY: wis digenerate');
      }
    }

    protected function checkUser(){
      $refresh = $this->option('refresh-token');
      if(!isset($this->btracker['install']['user']) || $refresh){
        if(!isset($this->btracker['user_token'])){
          $this->btracker['user_token'] = $this->ask('Setran Media User Token');
        }
        \Config::set('btracker',$this->btracker);
        try{
          $this->btracker['install']['user'] = $this->BTrackerApi->checkUser();
          $this->btracker['landing_page'] = $this->btracker['install']['user']['landing_page'];
          \Config::set('btracker',$this->btracker);
          $this->BTrackerApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->btracker['install']['user']);
            if ($this->confirm('Arep direset tokene?')) {
              unset($this->btracker['user_token']);
            }
            $this->checkUser();
          }
        }
      }else{
        $this->comment('*Setran Media User Authentication: wis divalidasi.');
      }
    }

    protected function checkDomain(){
      $refresh = $this->option('refresh-token');
      if(
        (
          isset($this->btracker['install']['user']['teams']) &&
          !isset($this->btracker['install']['domain'])
        ) || $refresh
      ){
        $this->btracker['landing_page'] = $this->btracker['install']['user']['landing_page'];
        if($this->btracker['install']['user']['teams']){
          if(!isset($this->btracker['type'])){
            $this->line('Tipe Projeke apa <comment>\'personal\', \'team\'</comment>');
            $this->btracker['type'] = $this->ask('Type');
            while(!in_array($this->btracker['type'],['personal','team'])){
              $this->error('Projek: '.$this->btracker['type'].' ora ana, sing bener ya lah!!!');
              $this->btracker['type'] = $this->ask('Type');
            }
          }
        }else{
          $this->btracker['type'] = 'personal';
        }

        if($this->btracker['type'] == 'team' && !isset($this->btracker['alibaba_team_id'])){
          $headers = ["#ID",'Name'];
          $teams = [];
          foreach($this->btracker['install']['user']['teams'] as $t){
            $teams[] = array_only($t,['id','name']);
          }
          $this->table($headers, $teams);
          $this->line('Lebokna ID Team');
          $this->btracker['alibaba_team_id'] = $this->ask('ID Team');
          while(!isset($this->btracker['install']['user']['teams'][$this->btracker['alibaba_team_id']])){
            $this->error('ID: '.$this->btracker['alibaba_team_id'].' ora ana, sing bener ya lah!!!');
            $this->btracker['alibaba_team_id'] = $this->ask('ID Team');
          }
        }
        if(
          $this->btracker['type'] == 'team' &&
          isset($this->btracker['alibaba_team_id']) &&
          isset($this->btracker['install']['user']['teams'][$this->btracker['alibaba_team_id']])
        ){
          $this->btracker['landing_page'] = $this->btracker['install']['user']['teams'][$this->btracker['alibaba_team_id']]['landing_page'];
        }

        if(!isset($this->btracker['domain'])){
          $this->comment('*Catatan: domain sing dadi dashboard adminne.');
          $this->btracker['domain'] = $this->ask('Domain');
        }
        \Config::set('btracker',$this->btracker);
        try{
          $this->btracker['install']['domain'] = $this->BTrackerApi->registerDomain();
          \Config::set('btracker',$this->btracker);
          $this->BTrackerApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->btracker['install']['domain']);
            if ($this->confirm('Arep direset data domaine?')) {
              unset($this->btracker['domain']);
              unset($this->btracker['type']);
              if(isset($this->btracker['alibaba_team_id'])) unset($this->btracker['alibaba_team_id']);
            }
            $this->checkDomain();
          }
        }
      }else{
        $this->comment('*Domain: wis terdaftar.');
      }
    }

    protected function migrateDB(){
      if(isset($this->btracker['install']['domain'])){
        $this->checkDbConnection();
        $this->checkMigration();
        if($this->db_connection && !$this->db_migrated){
          try{
            $this->call('migrate',['--force'=>true]);
          }catch(\Exception $e){
            $this->error('*Error : '.$e->getMessage());
          }
        }else{
          $this->line('<comment>*DB wis dimigrasi. Nek esih ana sing kurang jal running kiye</comment> \'<info>php artisan migrate</info>\'');
        }
      }
    }

    protected function createUser(){
      if(isset($this->btracker['install']['domain'])){
        $this->checkDbConnection();
        $this->checkMigration();
        if($this->db_connection && $this->db_migrated){
          $users = User::all(['id','name'])->count();
          if(!($users)){
            $this->call('batic:create-admin');
          }
        }
      }
    }

    protected function importDefault(){
      if(isset($this->btracker['install']['domain'])){
        if($this->db_connection && $this->db_migrated){
          if(!Seo::all(['id','name'])->count()){
            $this->call('batic:import-seos');
          }
          if(!Template::all(['id','name'])->count()){
            $this->call('batic:import-templates');
          }
          if(!Category::all(['id','name'])->count()){
            $this->call('batic:import-categories');
          }
          if(!Domain::where('domain',$this->btracker['domain'])->count()){
            $this->call('batic:create-domain',['--domain' => $this->btracker['domain']]);
          }
        }
      }
    }

    protected function checkDbConnection(){
        try {
            \DB::connection()->getPdo();
            $this->db_connection = true;
        }catch (\Exception $e) {
            $this->error('*Gagal konek meng DB : Jal cek konfigurasi DBne maning nang file \'.env\' wis bener apa urung.');
            $this->db_connection = false;
        }
    }

    protected function checkMigration(){
        if($this->db_connection){
            if(\Schema::hasTable('migrations'))
                $this->db_migrated = true;
        }
    }
}
