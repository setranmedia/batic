<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\BTracker\Api AS BTrackerApi;
use App\Queue;
use App\Keyword;

class QueueKeyword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batic:queue-keywords {--delay=}';
    protected $per_execution;
    protected $queue;
    protected $BTrackerApi;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate results and inject keywords from queues table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->per_execution = env('QUEUE_PER_EXECUTION',10);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $delay = $this->option('delay');
      if(!$delay) $delay = $this->ask('Delay pirang detik');
        try{
            $update = false;
            $this->comment('Sit...Enteni...Njiot random list queue...');
            $this->queue = Queue::where('finish',0)->inRandomOrder()->firstOrFail();

            $this->comment('Ngacak keyword...');
            $filename = 'queues'.DIRECTORY_SEPARATOR.$this->queue->id.'.txt';
            if(!\Storage::exists($filename)){
                $this->error('File keyword ora ketemu... dibusek yh?');
                die();
            }
            $keywords = json_decode(\Storage::get($filename),true);
            if(!is_array($keywords)){
                $this->error('Isi file keyword ora kewaca... Diedit nang koe yh?');
                die();
            }
            shuffle($keywords);
            $chunk = array_chunk($keywords, $this->per_execution);
            if(isset($chunk[0])){
                $this->BTrackerApi = new BTrackerApi(true);
                $banned_words   = \Storage::get('bad-words.txt');
                $arr_banned_words   = explode(',', $banned_words);
                if(is_array($arr_banned_words) && !empty($arr_banned_words)) $filter = true;
                else $filter = false;
                foreach($chunk[0] as $keyword){
                  sleep($delay);
                    if(!Keyword::where('query',$keyword)->first()){
                        $this->output->write('Mroses Keyword: <comment>'.$keyword.'</comment>...');
                        if($filter && if_check_banned($keyword,$arr_banned_words)){
                            $this->queue->failed += 1;
                            $this->queue->executed += 1;
                            $this->output->writeln('<error>Banned word</error>');
                        }else{
                          try{
                            $results = $this->executeProducts($keyword);
                            if(is_array($results)){
                                $this->saveKeyword($results);
                            }else{
                                $this->queue->failed += 1;
                                $this->queue->executed += 1;
                                $this->output->writeln('<error>Kosong</error>');
                            }
                          }catch(\Exception $e){
                            $this->queue->failed += 1;
                            $this->queue->executed += 1;
                            $this->output->writeln('<error>'.$e->getMessage().'</error>');
                          }
                        }
                    }else{
                        $this->queue->failed += 1;
                        $this->queue->executed += 1;
                        $this->output->writeln('<error>Wis ana</error>');
                    }
                }
                $update = true;
                array_forget($chunk, 0);
                if(is_array($chunk)){
                    $newKeywords = [];
                    foreach ($chunk as $value) {
                        $newKeywords = array_merge($newKeywords,$value);
                    }
                    if(!empty($newKeywords)){
                        $newKeywords = array_values($newKeywords);
                        \Storage::put($filename,json_encode($newKeywords));
                    }else{
                        $this->queue->finish = 1;
                    }
                }
                $this->queue->save();
            }
        }catch(\Exception $e){
            $this->error($e->getMessage());
        }

    }

    protected function executeProducts($q){
        try{
            $products = $this->BTrackerApi->getProducts(false,$q);
            return [
                'query' => $q,
                'type' => 'products',
                'results' => $products
            ];
        }catch(\Exception $e){
            if(env('RENDER_GOOGLE_IMAGES',false))
                return $this->executeGoogleImages($q);
            throw new \Exception($e->getMessage());
        }

    }

    protected function executeGoogleImages($q){
        try{
            $google_images = $this->BTrackerApi->getGoogleImages($q);
            return [
                'query' => $q,
                'type' => 'google_images',
                'results' => $google_images
            ];
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }

    protected function saveKeyword($keyword){
        try{
            Keyword::create($keyword);
            $this->queue->success += 1;
            $this->queue->executed += 1;
            $this->output->writeln('<info>OK</info> ('.$keyword['type'].')');
        }catch(\Exception $e){
            $this->queue->failed += 1;
            $this->queue->executed += 1;
            $this->output->writeln('<error>Error pas nyimpen</error>');
        }
    }
}
