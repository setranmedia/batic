<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BTracker\Api AS BTrackerApi;
use App\Category;

class GenerateProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batic:generate-products {--categories=} {--delay=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab Products for each categories';
    protected $categories = false;
    protected $BTrackerApi;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $delay = $this->option('delay');
        $this->checkCategories();
        if($this->categories){
            $this->BTrackerApi = new BTrackerApi;
            $total = count($this->categories);
            $i = 1;
            foreach ($this->categories as $category) {
                $this->output->write('Mroses Kategori <info>'.$i.'</info> kan <error>'.$total.'</error>: <comment>'.$category->name.'</comment>...');
                if($delay) sleep($delay);
                $results = $this->executeProducts($category->cid,$category->query);
                if($results && is_array($results)){
                    $this->output->writeln('<info>OK</info>');
                    $category->products = $results;
                    $category->save();
                }else{
                    $this->output->writeln('<error>Ora OK</error>');
                }
                $i++;
            }
        }
    }

    protected function checkCategories(){
        if(!$categories = $this->option('categories')){
            $this->line('Kategori apa sing arep digrab produke <comment>\'all\',\'parent\',\'childs\'</comment>');
            $categories = $this->ask('Kategori');
        }
        try {
            if($categories == 'all'){
                $categories = Category::all();
            }elseif($categories == 'parent'){
                $categories = Category::where('parent_id',NULL)->get();
            }elseif($categories == 'childs'){
                $categories = Category::where('parent_id','>',0)->get();
            }else{
                $categories = false;
            }

            if($categories){
                $this->categories = $categories;
            }else{
                $this->error('Error wa pas ngecek kategorine...');
                $this->categories = false;
            }
        }catch (\Exception $e) {
            $this->error($e->getMessage());
            $this->categories = false;
        }
    }

    protected function executeProducts($cid,$q){
        try{
            $products = $this->BTrackerApi->getProducts($cid,$q);
            return $products;
        }catch(\Exception $e){
            return false;
        }
    }
}
