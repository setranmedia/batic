<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class Template extends Model
{
	use Rememberable;
	
    protected $guarded = [];
	public $timestamps = false;
}
