<?php

function get_cached_products($category_id){
	$domain = config('domain');
	$key = $domain.'.products.'.$category_id;
	if($productsCat = cache($key)){
		return is_array($productsCat)?$productsCat:[];
	}
}

function is_layout($layout){
	if(is_array($layout)) return in_array(config('layout'),$layout);
	return config('layout') == $layout;
}

function shuffle_with_keys(&$array) {
	$aux = array();
	$keys = array_keys($array);
	shuffle($keys);
	foreach($keys as $key) {
		$aux[$key] = $array[$key];
		unset($array[$key]);
	}
	$array = $aux;
}

function array_random($arr, $num = 1) {
	if(!is_array($arr)){
		return false;
	}else{
		shuffle_with_keys($arr);
		$r = array();
		$i = 0;
		foreach($arr as $key => $val){
			if($num == 1){
				return $val;
				break;
			}
			if($i >= $num) break;
			$r[$key] = $val;
			$i++;
		}
		return $r;
	}
}

function random_keywords($count=5){
	$cache_time = env('CACHE_QUERY',0);
	$domain = config('domain');
	$keywords = App\Keyword::inRandomOrder()->limit($count)->remember($cache_time)->cacheTags($domain)->get();
	return $keywords && count($keywords) ? $keywords : false;
}

function getProductsByCategory(App\Category $category, App\Domain $site,$cache=true){
	$domain = $site->domain;
	$per_page = $site->per_page;
	$filename = 'products-cache/'.$domain.'/'.$category->id.'.txt';
	if($cache && \Storage::exists($filename)){
		$serialized = \Storage::get($filename);
		if(isJson($serialized)){
				return json_decode($serialized,true);
		}
	}
	if(is_array($category->products)){
		$productsCat = array_random($category->products,$site->per_page);
		$productsCat = is_array($productsCat)?$productsCat:false;
		if($productsCat) \Storage::put($filename,json_encode($productsCat));
		return $productsCat;
	}
}

function spp_get_delim($ref) {
	$search_engines = [
		'google.com'				=> 'q',
		'go.google.com'				=> 'q',
		'maps.google.com'			=> 'q',
		'local.google.com'			=> 'q',
		'search.yahoo.com'			=> 'p',
		'search.msn.com'			=> 'q',
		'bing.com'					=> 'q',
		'msxml.excite.com'			=> 'qkw',
		'search.lycos.com'			=> 'query',
		'alltheweb.com'				=> 'q',
		'search.aol.com'			=> 'query',
		'search.iwon.com'			=> 'searchfor',
		'ask.com'					=> 'q',
		'ask.co.uk'					=> 'ask',
		'search.cometsystems.com'	=> 'qry',
		'hotbot.com'				=> 'query',
		'overture.com'				=> 'Keywords',
		'metacrawler.com'			=> 'qkw',
		'search.netscape.com'		=> 'query',
		'looksmart.com'				=> 'key',
		'dpxml.webcrawler.com'		=> 'qkw',
		'search.earthlink.net'		=> 'q',
		'search.viewpoint.com'		=> 'k',
		'mamma.com'					=> 'query'
	];
	$delim = false;
	if (isset($search_engines[$ref])) {
		$delim = $search_engines[$ref];
	}else{
		$sub13 = substr($ref, 0, 13);
		if(substr($ref, 0, 7) == 'google.')
			$delim = "q";
		elseif($sub13 == 'search.atomz.')
			$delim = "sp-q";
		elseif(substr($ref, 0, 11) == 'search.msn.')
			$delim = "q";
		elseif($sub13 == 'search.yahoo.')
			$delim = "p";
		elseif(preg_match('/home\.bellsouth\.net\/s\/s\.dll/i', $ref))
			$delim = "bellsouth";
	}
	return $delim;
}

function spp_get_refer() {
	if (!isset($_SERVER['HTTP_REFERER']) || ($_SERVER['HTTP_REFERER'] == '')) return false;
	$referer_info = parse_url($_SERVER['HTTP_REFERER']);
	$referer = $referer_info['host'];
	if(substr($referer, 0, 4) == 'www.')
		$referer = substr($referer, 4);
	return $referer;
}

function spp_get_terms($d) {
	$terms       = null;
	$query_array = array();
	$query_terms = null;
	$query = @explode($d.'=', $_SERVER['HTTP_REFERER']);
	$query = @explode('&', $query[1]);
	$query = @urldecode($query[0]);
	$query = str_replace("'", '', $query);
	$query = str_replace('"', '', $query);
	$query_array = preg_split('/[\s,\+\.]+/',$query);
	$query_terms = implode(' ', $query_array);
	$terms = htmlspecialchars(urldecode(trim($query_terms)));
	return $terms;
}

function spp_setinfo() {
	$referer = spp_get_refer();
	if (!$referer) return false;
	$delimiter = spp_get_delim($referer);

	if($delimiter){
		$query = spp_get_terms($delimiter);
		$query = strtolower(trim($query));
		if($query && !empty($query)){
			try{
				$search = \App\Query::where('query',$query)->where('type','search-engine')->firstOrFail();
			}catch(\Exception $e){
				$search = new \App\Query(['query' => $query,'type' => 'search-engine']);
			}
			$search->total += 1;
			$search->save();
		}
	}
}

function if_check_badwords($string = "") {
    $banned_words   = \Storage::get('bad-words.txt');
    $arr_banned_words   = explode(',', $banned_words);
    return if_check_banned($string,$arr_banned_words);
}

function if_check_bad_ids($string = "") {
    $banned_ids   = \Storage::get('insfires/bad-ids.txt');
    $arr_banned_ids   = explode(',', $banned_ids);
    return if_check_banned($string,$arr_banned_ids);
}

function if_check_banned($string,$banneds) {
    $banned = false;
    $matches = array();
    $matchFound = preg_match_all("/\b(" . implode($banneds,"|") . ")\b/i",$string,$matches);
    if ($matchFound) {
        $words = array_unique($matches[0]);
        foreach($words as $word) {
            if ( 0 < strlen($word) ) {
                $banned = true;
            }
            else {
                $banned = false;
            }
        }
    }
    return $banned;
}
function isJson($string) {
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
}
