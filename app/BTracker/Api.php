<?php

namespace App\BTracker;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Exception;
use Carbon\Carbon;

class Api
{
	protected $base_url = 'http://alibaba.setranmedia.com';
	protected $type = 'default';
	protected $lang = 'en';
	protected $auth_url;
	protected $api_url;
	public $btracker;
	public $cache;
	public $cache_days = 30;

	public function __construct($cache = false){
		$this->api_url = $this->base_url.'/api';
		$this->auth_url = $this->base_url.'/api/authenticate';
		$this->btracker = config('btracker');
		$this->cache = $cache;
	}

	public function checkUser(){
		$user_token = config('btracker.user_token',false);
		if(!$user_token) return false;
		$client = new Client();
		$res = $client->request('GET', $this->api_url.'/check-user', [
			'query' => [
				'user_token' => $user_token,
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200) return $r;
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
		}
		throw new Exception('undefined_error');
	}

	public function registerDomain(){
		$btracker = config('btracker',false);
		if(!$btracker) return false;
		$client = new Client();
		$res = $client->request('GET', $this->api_url.'/add-domain', [
			'query' => $btracker,
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200) return true;
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
		}
		throw new Exception('undefined_error');
	}

	public function refreshToken(){
		$client = new Client();
		$btracker = config('btracker',false);
		if(!$btracker || !isset($btracker['domain'])) throw new Exception('Undefined configuration');
		$res = $client->request('GET', $this->api_url.'/refresh-token', [
			'query' => [
				'domain' => $btracker['domain'],
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($status == 200 && $this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = (json_decode($response,true));
			$btracker['user_token'] = $r['user_token'];
			\Config::set('btracker',$btracker);
			$this->saveConfig();
		}elseif($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = (json_decode($response,true));
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
			else throw new Exception($status);
		}else{
			throw new Exception('undefined_error');
		}
	}

	public function getProducts($cid,$query,$refresh=true){
		$client = new Client();

		$key = 'btracker.products';

		if($cid) $key .= '.'.$cid; else $cid = '';
		if($query) $key .= '.'.str_slug($query);

		if($this->cache && $cached = cache($key)){
			return $cached;
		}

		$failed_try = 0;
		$failed_try_cache_key = 'btracker.failed-get-products';
		if($failed_try_cache = cache($failed_try_cache_key)){
			$failed_try = $failed_try_cache;
			if($failed_try>20) throw new Exception('Something error with API');
		}
		$res = $client->request('GET', $this->api_url.'/grab', [
			'query' => [
				'user_token' => config('btracker.user_token'),
				'cid' => $cid,
				'q' => $query,
				'lang' => $this->lang,
				'method' => 'products',
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200 && is_array($r)){
				$banned_words   = \Storage::get('bad-words.txt');
				$arr_banned_words   = explode(',', $banned_words);
				if(is_array($arr_banned_words) && !empty($arr_banned_words)){
					foreach($r as $key => $result){
						if(if_check_banned($result['title'],$arr_banned_words)){
							unset($r[$key]);
						}
					}
				}
				cache([$key => $r], Carbon::now()->addDays($this->cache_days));
				\Cache::forget($failed_try_cache_key);
				return $r;
			}elseif($status == 400){
				try{
					$this->refreshToken();
					return $this->getProducts($cid,$query,false);
				}catch(\Exception $e){
					throw new Exception($e->getMessage());
				}
			}
			if(isset($r['error_message'])){
				throw new Exception($r['error_message']);
			}
		}
		$failed_try += 1;
		cache([$failed_try_cache_key => $failed_try], 360);
		throw new Exception('undefined_error');
	}

	public function getGoogleImages($query,$refresh=true){
		$client = new Client();
		$key = 'btracker.google_images';

		if($query) $key .= '.'.str_slug($query);

		if($this->cache && $cached = cache($key)){
			return $cached;
		}
		$res = $client->request('GET', $this->api_url.'/grab', [
			'query' => [
				'user_token' => config('btracker.user_token'),
				'q' => $query,
				'lang' => $this->lang,
				'method' => 'google_images',
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();

		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200 && is_array($r)){
				$banned_words   = \Storage::get('bad-words.txt');
				$arr_banned_words   = explode(',', $banned_words);
				if(is_array($arr_banned_words) && !empty($arr_banned_words)){
					foreach($r as $key => $result){
						if(if_check_banned($result['title'],$arr_banned_words)){
							unset($r[$key]);
						}
					}
				}
				cache([$key => $r], Carbon::now()->addDays($this->cache_days));
				\Cache::forget($failed_try_cache_key);
				return $r;
			}elseif($status == 400){
				try{
					$this->refreshToken();
					return $this->getGoogleImages($query,false);
				}catch(\Exception $e){
					throw new Exception($e->getMessage());
				}
			}
			if(isset($r['error_message'])){
				throw new Exception($r['error_message']);
			}
		}
		throw new Exception('undefined_error');
	}

	public function saveConfig(){
		$config = config('btracker',false);
		if(!$config) throw new Exception('undefined_error');
		if(!isset($config['version'])) $config['version'] = config('app.version','1.0.0');
		$save_data = var_export($config, 1);
		\File::put(config_path() . '/btracker.php', "<?php\n return $save_data ;");
	}

	public function offerJs($domain='undefined'){
		return '<script type="text/javascript">function redirectOffer(){var offer_url= '.json_encode(config('btracker.landing_page','https://newsonday.com')."/click?pid=".$this->lang."&cn=".config('btracker.type','undefined')."&cv=".config('btracker.domain',$domain)."&tp1=".$domain).';var url=jQuery(this).data( "url" );var title=jQuery(this).data( "title" );var itm_url= '.json_encode(config('btracker.landing_page','https://newsonday.com')).'+"/redirect?url="+url+"&title="+title;window.open(itm_url);window.location.href = offer_url;}</script>';
	}

	protected function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}
